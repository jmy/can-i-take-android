
Can I Take - An unfinished Android application evaluating various design concepts
===================================

This application was created in order to evaluate various design
patterns commonly used in complex Android applications.

The concept
-----------

The initial idea was to create a consumer application for optimizing
consumption of daily supplements, medicine, beverages, and meals so that
the benefits are maximized while at the same time minimizing any
possible side-effects. As an example, when curing iron deficiency,
ferrous sulfate tablets should be taken at least two hours after a meal
and one hour before a meal in order to maximize the iron absorption.
With the application you could track when you eat your beverages and
meals, and application would notify you when is the optimal time to take
supplements and medicine prescribed to you.

As the application turned more into an application design experiment,
the user interfaces in the current form is still extremely limited: you
can record any _consumable_ as consumed by tapping or dragging down the
button representing the consumable. You can add more consumables to
screen such as coffee or dinner by pressing the plus button on the
bottom right corner. According to the original UI concept the button
will would jump back up once there are no conflicts with it and any
other consumable. In addition, UI would disallow recording two
conflicting consumables at the same time.

Design
------

Some of the main sources for design inspiration of this project have been:

- [Clean Architecture by Uncle Bob](http://blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html)
    - Domain module consisting of a common business logic and application use cases aka _interactors_
        - pure java code
        - no dependencies to other modules
    - Presentation layer implementing all Android application user interface specific logic
        - including activities and fragments
    - Data module implementing backed specific functionality
        - implemented as an Android library
        - depends only on domain
        - persists domain (using data module specific POJOs)
        - communicates towards network (not yet implemented)
    - Dependencies only towards the "core" of the architecture
        - Inner domain part does not depend on any other parts
- Model View Presenter pattern
    - Thin activity and fragment classes
    - Main UI logic implemented in presenter classes
- Dependency/resource injection
    - Inter and intra-module services injected using [Dagger 2](https://github.com/google/dagger)
    - Android views and resources bound/injected using  [ButterKnife](https://github.com/JakeWharton/butterknife)

Implementation has been inspired a lot by [Android-Clean
Architecture](https://github.com/android10/Android-CleanArchitecture) by
Fernando Cejas.

Building and testing
--------------------

This application uses Gradle to automate building and testing.

Build and test the application from command line using the following command:

    ./gradlew build test

The following custom gradle tasks are provided for running unit tests selectively:

- runDomainUnitTests
- runDataUnitTests
- runUnitTests

Use `./gradlew <task>` to run any of the test tasks. You also use `test` tasks to run all tests.

For Android Studio users three shared test configurations are also provided to selectively run
module specific unit tests.

### Domain

Domain module is tested with unit test using following tools and libraries:

- JUnit
- Mockito
- Hamcrest

### Data

Domain module is tested with unit test using following tools and libraries:

- JUnit
- AssertJ
- Mockito
- Robolectric
- JSONAssert

### Presentation

The presentation module currently has only minimal testing. All acceptance tests testing the
application logic end-to-end are also still missing. Domain module is tested with unit test using
following tools and libraries:

- JUnit
- Mockito

TODO
------------

- support creating medication from UI
  - all UI created consumables are currently meals
- run domain and data logic in own thread (partly done in domain module)
- implement device-independent remote storage for consumption
  - maybe on a server
    - implement network communication using HTTP and JSON
- implement acceptance testing that test all modules through the application UI
- refactor presentation module to use own entity objects (uses currently entity classes from
domain layer)
- show only the latest or user selected consumables on the main activity
- persist a new not-yet-consumed items somehow
  - currently only consumed items are stored in model
- invent more "domain inspired"" names for modules and packages in spirit of _Clean Architecture_
  - the module and package structure should immediately reveal what this app is about, as opposed to
  telling what frameworks and patterns were used to implement this app

License
---------

       Copyright 2015 Solipaste Oy

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

           http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.
