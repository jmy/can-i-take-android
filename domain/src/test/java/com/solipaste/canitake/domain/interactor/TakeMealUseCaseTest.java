/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain.interactor;

import com.solipaste.canitake.domain.Consumed;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.Meal;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import com.solipaste.canitake.domain.repository.ConsumptionRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TakeMealUseCaseTest {

  @Mock private ConsumptionRepository mockConsumptionRepository;
  @Mock private Consumption mockResponseConsumption;
  private TakeMealUseCase takeMealUseCase;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    takeMealUseCase = new TakeMealUseCaseImpl(mockConsumptionRepository);
  }

  @Test public void takeMealUseCaseShouldCallBackOnErrorFromMeal() throws Exception {
    TakeMealUseCase.TakeMealCallback mockTakeMealCallback =
        mock(TakeMealUseCase.TakeMealCallback.class);
    final ErrorBundle mockErrorBundle = mock(ErrorBundle.class);

    final Meal mockMeal = mock(Meal.class);

    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((ConsumptionRepository.ConsumptionLoadCallback) invocation.getArguments()[0]).onConsumptionLoaded(
            mockResponseConsumption);
        return null;
      }
    }).when(mockConsumptionRepository)
        .getConsumption(any(ConsumptionRepository.ConsumptionLoadCallback.class));

    when(mockResponseConsumption.consume(mockMeal)).thenReturn(null);
    takeMealUseCase.takeMeal(mockMeal, mockTakeMealCallback);
    verify(mockTakeMealCallback).onError(any(ErrorBundle.class));
  }

  @Test public void takeMealUseCaseShouldCallBackOnErrorFromRepository() throws Exception {
    TakeMealUseCase.TakeMealCallback mockTakeMealCallback =
        mock(TakeMealUseCase.TakeMealCallback.class);
    final ErrorBundle mockErrorBundle = mock(ErrorBundle.class);

    final Meal mockMeal = mock(Meal.class);

    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((ConsumptionRepository.ConsumptionLoadCallback) invocation.getArguments()[0]).onConsumptionLoaded(
            mockResponseConsumption);
        return null;
      }
    }).when(mockConsumptionRepository)
        .getConsumption(any(ConsumptionRepository.ConsumptionLoadCallback.class));

    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((ConsumptionRepository.ConsumptionSaveCallback) invocation.getArguments()[1]).onError(
            mockErrorBundle);
        return null;
      }
    }).when(mockConsumptionRepository)
        .putConsumption(eq(mockResponseConsumption),
            any(ConsumptionRepository.ConsumptionSaveCallback.class));
    when(mockResponseConsumption.consume(mockMeal)).thenReturn(mock(Consumed.class));

    takeMealUseCase.takeMeal(mockMeal, mockTakeMealCallback);
    verify(mockTakeMealCallback).onError(mockErrorBundle);
  }

  @Test public void takeMealUseCaseShouldUpdateRepositoryAndMealAndCallbackSuccessfully()
      throws Exception {
    TakeMealUseCase.TakeMealCallback mockTakeMealCallback =
        mock(TakeMealUseCase.TakeMealCallback.class);
    final Meal mockMeal = mock(Meal.class);

    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((ConsumptionRepository.ConsumptionLoadCallback) invocation.getArguments()[0]).onConsumptionLoaded(
            mockResponseConsumption);
        return null;
      }
    }).when(mockConsumptionRepository)
        .getConsumption(any(ConsumptionRepository.ConsumptionLoadCallback.class));

    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((ConsumptionRepository.ConsumptionSaveCallback) invocation.getArguments()[1]).onConsumptionSaved();
        return null;
      }
    }).when(mockConsumptionRepository)
        .putConsumption(eq(mockResponseConsumption),
            any(ConsumptionRepository.ConsumptionSaveCallback.class));

    // return success from domain object
    when(mockResponseConsumption.consume(mockMeal)).thenReturn(mock(Consumed.class));

    takeMealUseCase.takeMeal(mockMeal, mockTakeMealCallback);

    verify(mockResponseConsumption).consume(mockMeal);
    verify(mockConsumptionRepository).putConsumption(eq(mockResponseConsumption),
        any(ConsumptionRepository.ConsumptionSaveCallback.class));
    verify(mockTakeMealCallback).onMealTaken(any(Consumed.class));
  }
}