/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain;


import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class MedicationTest {

  private Medication medicationWithoutName;
  private Medication iron;
  private Meal meal;

  @Before public void setUp() throws Exception {
    medicationWithoutName = new Medication();
    iron = new Medication("Iron");
    meal = new Meal("Lunch");
  }

  @Test public void getNameShouldReturnName() throws Exception {
    assertThat(iron.getName(), equalTo("Iron"));
  }

  @Test public void nameShouldBeEmptyByDefault() throws Exception {
    assertThat(medicationWithoutName.getName(), equalTo(""));
  }

  @Test public void nameShouldChangeAfterSetName() throws Exception {
    iron.setName("Vitamin D");
    assertThat(iron.getName(), equalTo("Vitamin D"));
  }

  @Test public void nameShouldBeEmptyAfterSettingItToEmpty() throws Exception {
    iron.setName("");
    assertThat(iron.getName(), equalTo(""));
  }

  @Test public void shouldReturnZeroMedicationConsumptionLimitsWhenAsked() throws Exception {
    final Medication vitamin = new Medication("vitamin", 60);
    assertThat(vitamin.getSafeToConsumeAfterMinutes(new Meal("breakfast")), equalTo(60));
  }

  @Test public void shouldReturnMealConsumptionLimitsWhenAsked() throws Exception {
    final Medication vitamin = new Medication("vitamin", 60);
    assertThat(vitamin.getSafeToConsumeAfterMinutes(new Medication("iron")), equalTo(0));
  }

  @Test public void shouldBeAbleToCreateDVitamin() throws Exception {
    final Medication vitaminD = Medication.getVitamin("D");
    assertThat(vitaminD.getName(), equalTo("Vitamin D"));
    assertThat(vitaminD.getSafeToConsumeAfterMinutes(meal), equalTo(0));
  }

  @Test public void shouldBeAbleToCreateGenericVitamin() throws Exception {
    final Medication vitamin = Medication.getVitamin();
    assertThat(vitamin.getName(), equalTo("Generic vitamin"));
    assertThat(vitamin.getSafeToConsumeAfterMinutes(meal), equalTo(0));
  }

  @Test public void shouldBeAbleToCreateFerrousSulfate() throws Exception {
    final Medication ferrousSulfate = Medication.getFerrousSulfate();
    assertThat(ferrousSulfate.getName(), equalTo("Ferrous sulfate"));
    assertThat(ferrousSulfate.getSafeToConsumeAfterMinutes(meal), equalTo(120));
  }
}