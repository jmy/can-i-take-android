/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain;

import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;

public class ConsumptionTest {

  private Medication iron;
  private Meal meal;
  private Consumption consumption;
  private Medication vitaminD;

  @Before public void setUp() throws Exception {
    iron = new Medication("Iron", 120);
    vitaminD = new Medication("Vitamin D", 0);
    meal = new Meal("Lunch", 60);
    consumption = new Consumption();
  }

  private Calendar getTimeBeforeNow(int hours, int minutes) {
    final Calendar timeBefore = Calendar.getInstance();
    timeBefore.add(Calendar.HOUR, -hours);
    timeBefore.add(Calendar.MINUTE, -minutes);
    return timeBefore;
  }

  @Test public void ironMayBeConsumedTwoHoursAfterMeal() throws Exception {
    final Calendar twoHoursAgo = getTimeBeforeNow(2, 0);
    consumption.consumeAt(meal, twoHoursAgo);
    assertThat(consumption.canConsumeNow(iron), is(true));
  }

  @Test public void ironMayBeConsumedMoreThanTwoHourAfterMeal() throws Exception {
    final Calendar moreThanTwoHoursAgo = getTimeBeforeNow(2, 1);
    consumption.consumeAt(meal, moreThanTwoHoursAgo);
    assertThat(consumption.canConsumeNow(iron), is(true));
  }

  @Test public void ironShouldNotBeConsumedAlmostTwoHoursAfterMeal() throws Exception {
    final Calendar almostTwoHoursAgo = getTimeBeforeNow(2, -1);
    consumption.consumeAt(meal, almostTwoHoursAgo);
    assertThat(consumption.canConsumeNow(iron), is(false));
  }

  @Test public void ironShouldNotBeConsumedHalfAnHourAfterMeal() throws Exception {
    final Calendar halfAnHourAgo = getTimeBeforeNow(0, 30);
    consumption.consumeAt(meal, halfAnHourAgo);
    assertThat(consumption.canConsumeNow(iron), is(false));
  }

  @Test public void ironMayBeConsumedToEmptyStomach() throws Exception {
    assertThat(consumption.canConsumeNow(iron), is(true));
  }

  @Test public void vitaminDMayBeConsumedToEmptyStomach() throws Exception {
    assertThat(consumption.canConsumeNow(vitaminD), is(true));
  }

  @Test public void vitaminDMayBeConsumedImmediatelyAfterMeal() throws Exception {
    consumption.consume(meal);
    assertThat(consumption.canConsumeNow(vitaminD), is(true));
  }

  @Test public void mealCannotBeConsumedImmediatelyAfterIron() throws Exception {
    consumption.consume(iron);
    assertThat(consumption.canConsumeNow(meal), is(false));
  }

  @Test public void mealCannotBeConsumedAlmostOneHourAfterIron() throws Exception {
    final Calendar almostOneHourAgo = getTimeBeforeNow(1, -1);
    consumption.consumeAt(iron, almostOneHourAgo);
    assertThat(consumption.canConsumeNow(meal), is(false));
  }

  @Test public void mealMayBeConsumedOneHourAfterIron() throws Exception {
    final Calendar oneHourAgo = getTimeBeforeNow(1, 0);
    consumption.consumeAt(iron, oneHourAgo);
    assertThat(consumption.canConsumeNow(meal), is(true));
  }

  @Test public void mealMayBeConsumedImmediatelyAfterVitaminD() throws Exception {
    consumption.consume(vitaminD);
    assertThat(consumption.consume(meal), isA(Consumed.class));
  }

  @Test public void consumableShouldBeAccessibleInSameOrderAfterConsumed() throws Exception {
    final Consumed mealEntry = consumption.consume(meal);
    final Consumed ironEntry = consumption.consume(iron);
    final Meal dinner = new Meal("Dinner");
    final Consumed dinnerEntry = consumption.consume(dinner);
    final Medication superVitamin = new Medication("SuperVitamin");
    final Consumed superVitaminEntry = consumption.consume(superVitamin);
    assertThat(consumption.getConsumed(), hasSize(4));
    assertThat(consumption.getConsumed(),
        contains(mealEntry, ironEntry, dinnerEntry, superVitaminEntry));
  }

  @Test public void mealsShouldNotBeFoundInNewConsumption() throws Exception {
    assertThat(consumption.getConsumed(), empty());
  }
}