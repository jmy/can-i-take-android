/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain.interactor;

import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import com.solipaste.canitake.domain.interactor.GetConsumptionUseCase.GetConsumptionCallback;
import com.solipaste.canitake.domain.repository.ConsumptionRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class GetConsumptionUseCaseTest {

  @Mock private ConsumptionRepository mockConsumptionRepository;
  @Mock private PostExecutionThread mockPostExecutionThread;
  @Mock private UseCaseExecutor mockUseCaseExecutor;
  private GetConsumptionUseCaseImpl getConsumptionUseCase;
  private GetConsumptionCallback mockGetConsumptionCallback;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    getConsumptionUseCase = new GetConsumptionUseCaseImpl(mockConsumptionRepository);
    mockGetConsumptionCallback = mock(GetConsumptionCallback.class);
    getConsumptionUseCase.useCaseExecutor = mockUseCaseExecutor;
    getConsumptionUseCase.postExecutionThread = mockPostExecutionThread;
  }

  @Test public void testGetConsumptionUseCaseExecutes() throws Exception {
    getConsumptionUseCase.getConsumption(mockGetConsumptionCallback);

    verify(mockUseCaseExecutor).execute(any(Runnable.class));
  }

  @Test public void testGetConsumptionUseCaseCallbackSuccessful() throws Exception {
    final Consumption mockResponseConsumption = mock(Consumption.class);

    // mock success from repository
    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((ConsumptionRepository.ConsumptionLoadCallback) invocation.getArguments()[0]).onConsumptionLoaded(
            mockResponseConsumption);
        return null;
      }
    }).when(mockConsumptionRepository)
        .getConsumption(any(ConsumptionRepository.ConsumptionLoadCallback.class));

    getConsumptionUseCase.getConsumption(mockGetConsumptionCallback); // should set callback
    getConsumptionUseCase.run();

    // we can only verify thread posting
    verify(mockPostExecutionThread).post(any(Runnable.class));
  }

  @Test public void testGetConsumptionUseCaseCallbackError() throws Exception {
    final ErrorBundle mockErrorBundle = mock(ErrorBundle.class);

    // mock error from repository
    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((ConsumptionRepository.ConsumptionLoadCallback) invocation.getArguments()[0]).onError(
            mockErrorBundle);
        return null;
      }
    }).when(mockConsumptionRepository)
        .getConsumption(any(ConsumptionRepository.ConsumptionLoadCallback.class));

    getConsumptionUseCase.getConsumption(mockGetConsumptionCallback); // should set callback
    getConsumptionUseCase.run();

    // we can only verify thread posting
    verify(mockPostExecutionThread).post(any(Runnable.class));
  }

  @Test public void testGetConsumptionCallsRepositoryWhenRun() throws Exception {
    final ConsumptionRepository.ConsumptionLoadCallback mockRepositoryConsumptionLoadCallback =
        mock(ConsumptionRepository.ConsumptionLoadCallback.class);

    getConsumptionUseCase.run();

    verify(mockConsumptionRepository).getConsumption(
        any(ConsumptionRepository.ConsumptionLoadCallback.class));
  }
}