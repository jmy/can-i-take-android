/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class MealTest {

  @Test public void getName() throws Exception {
    final Meal dinner = new Meal("dinner");
    assertThat(dinner.getName(), equalTo("dinner"));
  }

  @Test public void shouldReturnNameWhenAsked() throws Exception {
    final Meal dinner = new Meal("dinner");
    assertThat(dinner.toString(), equalTo("dinner"));
  }

  @Test public void shouldReturnMedicationConsumptionLimitsWhenAsked() throws Exception {
    final Meal dinner = new Meal("dinner", 30);
    assertThat(dinner.getSafeToConsumeAfterMinutes(new Medication()), equalTo(30));
  }

  @Test public void shouldReturnZeroMealConsumptionLimitsWhenAsked() throws Exception {
    final Meal dinner = new Meal("dinner", 30);
    assertThat(dinner.getSafeToConsumeAfterMinutes(new Meal("breakfast")), equalTo(0));
  }
}