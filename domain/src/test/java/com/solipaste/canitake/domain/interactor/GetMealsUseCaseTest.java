/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain.interactor;

import com.solipaste.canitake.domain.Consumed;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.Meal;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import com.solipaste.canitake.domain.repository.ConsumptionRepository;
import edu.umd.cs.findbugs.annotations.NonNull;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class GetMealsUseCaseTest {

  @Mock private ConsumptionRepository mockConsumptionRepository;
  private GetMealsUseCaseImpl getMealsUseCase;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    getMealsUseCase = new GetMealsUseCaseImpl(mockConsumptionRepository);
  }

  @Test public void mealsShouldBeReceived() throws Exception {
    GetMealsUseCase mUseCase = new GetMealsUseCaseImpl(new ConsumptionRepository() {
      @Override public void getConsumption(ConsumptionLoadCallback consumptionCallback) {
        final Consumption consumption = new Consumption();
        consumption.consume(new Meal("breakfast"));
        consumption.consume(new Meal("supper"));
        consumptionCallback.onConsumptionLoaded(consumption);
      }

      @Override public void putConsumption(@NonNull Consumption consumption,
          @NonNull ConsumptionSaveCallback consumptionSaveCallback) {
        throw new AssertionError();
      }
    });

    final GetMealsUseCase.GetMealsCallback getMealsCallback =
        new GetMealsUseCase.GetMealsCallback() {
          @Override public void onMealsLoaded(List<Consumed> meals) {
            assertThat(meals, hasSize(2));
          }

          @Override public void onError(@NonNull ErrorBundle errorBundle) {
            throw new AssertionError();
          }
        };
    mUseCase.getMeals(getMealsCallback);
  }

  @Test public void testGetMealsUseCaseCallbackSuccessful() throws Exception {
    GetMealsUseCase.GetMealsCallback mockGetMealsCallback =
        mock(GetMealsUseCase.GetMealsCallback.class);
    @SuppressWarnings("unchecked")
    final List<Consumed> mockResponseConsumedList = (List<Consumed>) mock(List.class);
    final Consumption mockResponseConsumption = mock(Consumption.class);

    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((ConsumptionRepository.ConsumptionLoadCallback) invocation.getArguments()[0]).onConsumptionLoaded(
            mockResponseConsumption);
        return null;
      }
    }).when(mockConsumptionRepository)
        .getConsumption(any(ConsumptionRepository.ConsumptionLoadCallback.class));

    when(mockResponseConsumption.getConsumed()).thenReturn(mockResponseConsumedList);
    getMealsUseCase.getMeals(mockGetMealsCallback);

    verify(mockGetMealsCallback).onMealsLoaded(anyListOf(Consumed.class));
    verifyNoMoreInteractions(mockGetMealsCallback);
    verifyZeroInteractions(mockResponseConsumedList);
  }

  @Test public void testGetMealsUseCaseCallbackError() throws Exception {
    GetMealsUseCase.GetMealsCallback mockGetMealsCallback =
        mock(GetMealsUseCase.GetMealsCallback.class);
    @SuppressWarnings("unchecked")
    final List<Consumed> mockResponseConsumedList = (List<Consumed>) mock(List.class);
    final Consumption mockResponseConsumption = mock(Consumption.class);
    final ErrorBundle mockErrorBundle = mock(ErrorBundle.class);

    doAnswer(new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        ((ConsumptionRepository.ConsumptionLoadCallback) invocation.getArguments()[0]).onError(
            mockErrorBundle);
        return null;
      }
    }).when(mockConsumptionRepository)
        .getConsumption(any(ConsumptionRepository.ConsumptionLoadCallback.class));

    when(mockResponseConsumption.getConsumed()).thenReturn(mockResponseConsumedList);
    getMealsUseCase.getMeals(mockGetMealsCallback);

    verify(mockGetMealsCallback).onError(mockErrorBundle);
    verify(mockGetMealsCallback, never()).onMealsLoaded(mockResponseConsumedList);
    verifyNoMoreInteractions(mockGetMealsCallback);
    verifyZeroInteractions(mockErrorBundle);
  }

  @Test public void mealsShouldNotBeReceivedInCaseOfRepositoryError() throws Exception {
    GetMealsUseCase mUseCase = new GetMealsUseCaseImpl(new ConsumptionRepository() {
      @Override public void getConsumption(ConsumptionLoadCallback consumptionCallback) {
        consumptionCallback.onError(new ErrorBundle() {
          @Override public Exception getException() {
            return new Exception();
          }

          @Override public String getErrorMessage() {
            throw new AssertionError();
          }
        });
      }

      @Override public void putConsumption(@NonNull Consumption consumption,
          @NonNull ConsumptionSaveCallback consumptionSaveCallback) {
        throw new AssertionError();
      }

    });

    final GetMealsUseCase.GetMealsCallback getMealsCallback =
        new GetMealsUseCase.GetMealsCallback() {
          @Override public void onMealsLoaded(List<Consumed> meals) {
            throw new AssertionError();
          }

          @Override public void onError(@NonNull ErrorBundle errorBundle) {
            assertThat(errorBundle.getException(), isA(Exception.class));
          }
        };
    mUseCase.getMeals(getMealsCallback);
  }

  @Test public void testGetMealsExecutionWithMock() throws Exception {
    GetMealsUseCase.GetMealsCallback mockGetMealsCallback =
        mock(GetMealsUseCase.GetMealsCallback.class);

    getMealsUseCase.getMeals(mockGetMealsCallback);

    verify(mockConsumptionRepository).getConsumption(
        any(ConsumptionRepository.ConsumptionLoadCallback.class));
    verifyNoMoreInteractions(mockGetMealsCallback);
    verifyZeroInteractions(mockConsumptionRepository);
  }
}