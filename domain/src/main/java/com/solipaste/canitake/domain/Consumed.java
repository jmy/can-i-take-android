/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain;

import edu.umd.cs.findbugs.annotations.NonNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Consumed implements java.io.Serializable {
  private static final Logger log = Logger.getLogger(Consumed.class.getSimpleName());
  private static final DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss:S");

  private static final long serialVersionUID = 6668329143949025153L;

  private final AbstractMap.SimpleImmutableEntry<Calendar, Consumable> entry;

  /**
   * Creates a new {@code Consumed} at specified time and consumable.
   *
   * @param time the time when consumable was consumed.
   * @param consumable the consumable that was consumed
   */
  public Consumed(Calendar time, Consumable consumable) {
    this.entry = new AbstractMap.SimpleImmutableEntry<>(time, consumable);
  }

  /**
   * Creates a new {@code Consumed} using the specified entry.
   *
   * @param entry the entry to copy
   */
  public Consumed(Map.Entry<? extends Calendar, ? extends Consumable> entry) {
    this.entry = new AbstractMap.SimpleImmutableEntry<>(entry);
  }

  /**
   * Returns the time when this item was consumed.
   *
   * @return the time when this item was consumed
   */
  public Calendar getTime() {
    return entry.getKey();
  }

  /**
   * Returns the consumable.
   *
   * @return the consumable
   */
  public Consumable getConsumable() {
    return entry.getValue();
  }

  public boolean equals(Object o) {
    if (!(o instanceof Consumed)) return false;
    Consumed e = (Consumed) o;
    return eq(getTime(), e.getTime()) && eq(getConsumable(), e.getConsumable());
  }

  /**
   * Returns the hash code value for this entry.
   *
   * @return the hash code value for this entry
   * @see #equals
   */
  public int hashCode() {
    return entry.hashCode();
  }

  /**
   * Returns a String representation of this entry.  This
   * implementation returns the string representation of
   * time followed by the equals character ("<tt>=</tt>")
   * followed by the string representation of meal.
   *
   * @return a String representation of this entry
   */
  public String toString() {
    return String.format("%s = %s: \"%s\"", getFormattedTime(getTime().getTime()),
        entry.getValue().getClass().getSimpleName(), entry.getValue());
  }

  /**
   * Tests if the specified consumableCandidate would be OK to be consumed after
   * this consumable.
   *
   * @return {@code true} if consumableCandidate may be consumed, otherwise
   * {@code false}.
   */
  public boolean couldHaveBeenConsumedBefore(Consumable consumableCandidate) {

    Calendar safeToConsumeAfter = Calendar.getInstance();
    final int safeToConsumeAfterMinutes =
        consumableCandidate.getSafeToConsumeAfterMinutes(getConsumable());
    safeToConsumeAfter.add(Calendar.MINUTE, -safeToConsumeAfterMinutes);

    log.log(Level.FINE, "Compare if {0} is before {1}", new Object[] {
        getFormattedTime(getTime().getTime()), getFormattedTime(safeToConsumeAfter.getTime())
    });

    // allow equal times so check with negation of after
    final boolean canConsumeBefore = !getTime().after(safeToConsumeAfter);
    return canConsumeBefore;
  }

  @NonNull synchronized private String getFormattedTime(Date time) {
    return timeFormat.format(time);
  }

  /**
   * Utility method for testing equality, checking for nulls.
   */
  private static boolean eq(Object o1, Object o2) {
    return o1 == null ? o2 == null : o1.equals(o2);
  }
}
