/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain.interactor;

/**
 * Interface representing executor that can be used to execute a use case asynchronously in some
 * environment specific way.
 */
public interface UseCaseExecutor {

  /**
   * Executes the runnable use case asynchronously.
   *
   * @param runnable the use case to be executed.
   */
  void execute(Runnable runnable);
}
