/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain.interactor;

import com.solipaste.canitake.domain.Consumed;
import com.solipaste.canitake.domain.Meal;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import edu.umd.cs.findbugs.annotations.NonNull;

public interface TakeMealUseCase {

  /**
   * Executes take meal use case.
   *
   * @param callback A callback that is called when results are available
   */
  void takeMeal(@NonNull Meal meal, @NonNull TakeMealCallback callback);

  /**
   * Callback used to notify caller of the results of TakeMealUseCase operation
   */
  interface TakeMealCallback {
    void onMealTaken(@NonNull Consumed consumed);

    void onError(@NonNull ErrorBundle errorBundle);
  }
}
