/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain.interactor;

import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import com.solipaste.canitake.domain.repository.ConsumptionRepository;
import edu.umd.cs.findbugs.annotations.NonNull;
import javax.inject.Inject;

/**
 * Concrete class implementing Get Consumption use case in asynchronous manner.
 */
public class GetConsumptionUseCaseImpl implements GetConsumptionUseCase, Runnable {
  @NonNull private final ConsumptionRepository consumptionRepository;

  @Inject UseCaseExecutor useCaseExecutor;
  @Inject PostExecutionThread postExecutionThread;
  private GetConsumptionCallback getConsumptionCallback;

  /**
   * Creates a new {@code GetConsumptionUseCaseImpl} with specified repository.
   *
   * @param repository the ConsumptionRepository Used to read consumption from.
   */
  @Inject public GetConsumptionUseCaseImpl(@NonNull ConsumptionRepository repository) {
    consumptionRepository = repository;
  }

  /**
   * Executes Get consumption use case asynchronously.
   *
   * @param getConsumptionCallback A {@link GetConsumptionCallback} used to notify the caller
   */
  @Override
  public void getConsumption(@NonNull final GetConsumptionCallback getConsumptionCallback) {
    this.getConsumptionCallback = getConsumptionCallback;

    useCaseExecutor.execute(this);
  }

  // Used internally only
  @Override public void run() {
    consumptionRepository.getConsumption(repositoryCallback);
  }

  private final ConsumptionRepository.ConsumptionLoadCallback repositoryCallback =
      new ConsumptionRepository.ConsumptionLoadCallback() {

        @Override public void onConsumptionLoaded(@NonNull final Consumption consumption) {
          postExecutionThread.post(new Runnable() {
            @Override public void run() {
              getConsumptionCallback.onConsumptionLoaded(consumption);
            }
          });
        }

        @Override public void onError(@NonNull final ErrorBundle errorBundle) {
          postExecutionThread.post(new Runnable() {
            @Override public void run() {
              getConsumptionCallback.onError(errorBundle);
            }
          });
        }
      };
}
