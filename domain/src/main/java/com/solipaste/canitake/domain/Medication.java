/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain;

import edu.umd.cs.findbugs.annotations.NonNull;

/**
 * A concrete Consumable class for storing medication of various types.
 */
public class Medication implements Consumable {

  @NonNull private String name = "";
  private int safeToConsumeAfterMealMinutes;

  /**
   * Creates a new {@code Medication} with empty name. The safe consumption duration is set to
   * zero.
   */
  public Medication() {
  }

  /**
   * Creates a new {@code Medication} with specified name. The safe consumption duration is set to
   * zero.
   *
   * @param name the name for the medication.
   */
  public Medication(@NonNull String name) {
    this.name = name;
  }

  /**
   * Creates a new {@code Medication} with specified name and safe consumption duration.
   *
   * @param name the name for the medication.
   * @param safeToConsumeAfterMealMinutes the minimum duration in minutes between some
   * meal and this medication.
   */
  public Medication(@NonNull String name, int safeToConsumeAfterMealMinutes) {
    this.name = name;
    this.safeToConsumeAfterMealMinutes = safeToConsumeAfterMealMinutes;
  }

  /**
   * Creates a new vitamin {@code Medication} with no consumption limits.
   *
   * @return the created vitamin.
   */
  public static Medication getVitamin() {
    return new Medication("Generic vitamin", 0);
  }

  /**
   * Creates a new vitamin {@code Medication} with specified name and no consumption limits.
   *
   * @param type the type of the vitamin
   * @return The created vitamin.
   */
  public static Medication getVitamin(String type) {
    return new Medication("Vitamin " + type, 0);
  }

  /**
   * Creates a new Ferrous sulfate supplement {@code Medication} with recommended consumption
   * limits.
   *
   * @return Returns the created supplement.
   */
  public static Medication getFerrousSulfate() {
    return new Medication("Ferrous sulfate", 120);
  }

  /**
   * Returns the name of the medication.
   *
   * @return The name of the medication.
   */
  @Override @NonNull public String getName() {
    return name;
  }

  /**
   * Sets the name of the medication.
   *
   * @param name the new name for the medication
   */
  @Override public void setName(@NonNull String name) {
    this.name = name;
  }

  /**
   * Returns the duration after this medication is safe to be consumed after the specified another
   * consumable. As an example, an ferrous sulfate supplement should be consumed at least 120
   * minutes after a meal.
   *
   * @return duration in minutes.
   */
  @Override public int getSafeToConsumeAfterMinutes(Consumable consumable) {
    if (consumable.getClass().equals(Meal.class)) {
      return safeToConsumeAfterMealMinutes;
    } else {
      return 0;
    }
  }

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Medication that = (Medication) o;

    if (safeToConsumeAfterMealMinutes != that.safeToConsumeAfterMealMinutes) {
      return false;
    }
    return name.equals(that.name);
  }

  @Override public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + safeToConsumeAfterMealMinutes;
    return result;
  }
}
