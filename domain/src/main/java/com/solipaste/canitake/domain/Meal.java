/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain;

import edu.umd.cs.findbugs.annotations.NonNull;

/**
 * A concrete Consumable class for storing meals of various types.
 */
public class Meal implements Consumable {
  @NonNull private String name;
  private int safeToConsumeAfterMedicationMinutes;

  /**
   * Creates a new {@code Meal} with specified name. The safe consumption duration is set to zero.
   *
   * @param name the name for the meal.
   */
  public Meal(@NonNull String name) {
    this.name = name;
  }

  /**
   * Creates a new {@code Meal} with specified name and safe consumption duration.
   *
   * @param name The name for the meal.
   * @param safeToConsumeAfterMedicationMinutes The minimum duration in minutes between some
   * medication and this meal.
   */
  public Meal(@NonNull String name, int safeToConsumeAfterMedicationMinutes) {
    this.name = name;
    this.safeToConsumeAfterMedicationMinutes = safeToConsumeAfterMedicationMinutes;
  }

  /**
   * Returns the name of the meal.
   *
   * @return The name of the meal.
   */
  @Override @NonNull public String getName() {
    return name;
  }

  /**
   * Sets name of the meal.
   *
   * @param name the new name for the meal.
   */
  @Override public void setName(@NonNull String name) {
    this.name = name;
  }

  /**
   * Returns the string representation of the meal.
   *
   * @return the string representation of the meal.
   */
  @Override public @NonNull String toString() {
    return name;
  }

  /**
   * Returns the duration after this meal is safe to be consumed after the specified another
   * consumable. As an example, an meal should be consumed at least 60 minutes after a
   * iron supplement.
   *
   * @return duration in minutes.
   */
  @Override public int getSafeToConsumeAfterMinutes(@NonNull Consumable consumable) {
    if (consumable.getClass().equals(Medication.class)) {
      return safeToConsumeAfterMedicationMinutes;
    } else {
      return 0;
    }
  }

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Meal meal = (Meal) o;

    if (safeToConsumeAfterMedicationMinutes != meal.safeToConsumeAfterMedicationMinutes) {
      return false;
    }

    return name.equals(meal.name);
  }

  @Override public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + safeToConsumeAfterMedicationMinutes;
    return result;
  }
}
