/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain.interactor;

import com.solipaste.canitake.domain.Consumed;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.Meal;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import com.solipaste.canitake.domain.repository.ConsumptionRepository;
import edu.umd.cs.findbugs.annotations.NonNull;
import javax.inject.Inject;

/**
 * Concrete implementation of TakeMeal use case in synchronous manner.
 *
 * TODO: Refactor implementation to perform operation asynchronously.
 */
public class TakeMealUseCaseImpl implements TakeMealUseCase {
  @NonNull private final ConsumptionRepository consumptionRepository;

  /**
   * Creates a new {@code TakeMealUseCaseImpl} with specified repository.
   *
   * @param consumptionRepository the ConsumptionRepository Used to read meals from.
   */
  @Inject public TakeMealUseCaseImpl(@NonNull final ConsumptionRepository consumptionRepository) {
    this.consumptionRepository = consumptionRepository;
  }

  /**
   * Executes Get meals use case synchronously.
   *
   * @param meal the meal to be consumed
   * @param callback {@link TakeMealUseCase.TakeMealCallback} used to notify the caller
   */
  @Override
  public void takeMeal(@NonNull final Meal meal, @NonNull final TakeMealCallback callback) {
    consumptionRepository.getConsumption(new ConsumptionRepository.ConsumptionLoadCallback() {
      @Override public void onConsumptionLoaded(@NonNull Consumption consumption) {
        TakeMealUseCaseImpl.this.takeMeal(consumption, meal, callback);
      }

      @Override public void onError(@NonNull ErrorBundle errorBundle) {
        callback.onError(errorBundle);
      }
    });
  }

  private void takeMeal(@NonNull Consumption consumption, Meal meal, @NonNull final TakeMealCallback callback) {
    final Consumed consumed = consumption.consume(meal);
    if (consumed != null) {
      consumptionRepository.putConsumption(consumption,
          new ConsumptionRepository.ConsumptionSaveCallback() {
            @Override public void onConsumptionSaved() {
              callback.onMealTaken(consumed);
            }

            @Override public void onError(@NonNull ErrorBundle errorBundle) {
              callback.onError(errorBundle);
            }
          });
    } else {
      callback.onError(new ErrorBundle() {
        private final IllegalStateException exception =
            new IllegalStateException("taking meal" + " failed");

        @Override public Exception getException() {
          return exception;
        }

        @Override public String getErrorMessage() {
          return exception.getMessage();
        }
      });
    }
  }
}
