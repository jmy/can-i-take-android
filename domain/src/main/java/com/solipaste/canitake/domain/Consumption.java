/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * {@code Consumption} is class for storing consumption of consumables. This class allows you to
 * record the time and type of consumable consumed.
 */
public class Consumption {
  static final DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
  private static final Logger log = Logger.getLogger(Consumption.class.getSimpleName());
  private final List<Consumed> consumedList = new ArrayList<>();

  /**
   * Creates a new {@code Consumption}.
   */
  public Consumption() {
  }

  /**
   * Consumes specified consumable immediately.
   *
   * @param consumed the consumable to be consumed
   */
  public Consumed consume(Consumable consumed) {
    final Consumed consumedEntry = new Consumed(Calendar.getInstance(), consumed);
    log.log(Level.FINE, "Consumable consumed at {0}",
        timeFormat.format(consumedEntry.getTime().getTime()));
    if (consumedList.add(consumedEntry)) {
      return consumedEntry;
    } else {
      return null;
    }
  }

  /**
   * Consumes specified consumable at some specific time. The time specified can be in the past or
   * in the future.
   *
   * @param consumable the consumable to be consumed
   * @param time the time when consumable is to be consumed
   */
  public Consumed consumeAt(Consumable consumable, Calendar time) {
    final Consumed entry = new Consumed(time, consumable);
    log.log(Level.FINE, "Consumable consumed at {0}", timeFormat.format(entry.getTime().getTime()));
    if (consumedList.add(entry)) {
      return entry;
    } else {
      return null;
    }
  }

  /**
   * Tests if a consumable can be consumed how.
   *
   * @param consumable the consumable to be tested
   *
   * @return {@code true} if it is ok to consume the consumable, {@code false} otherwise
   */
  public boolean canConsumeNow(Consumable consumable) {
    for (Consumed entry : consumedList) {
      if (!entry.couldHaveBeenConsumedBefore(consumable)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Gets list of all consumable objects that have been consumed so far.
   *
   * @return the list of consumed objects.
   */
  public List<Consumed> getConsumed() {
    return consumedList;
  }
}
