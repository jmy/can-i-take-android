/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain.repository;

import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import edu.umd.cs.findbugs.annotations.NonNull;

/**
 * {@code ConsumptionRepository} represents a permanent storage for {@link Consumption}.
 */
public interface ConsumptionRepository {

  /**
   * Returns Consumption by calling the callback provided as a argument.
   *
   * @param consumptionLoadCallback callback called to return consumption or error.
   */
  void getConsumption(final ConsumptionLoadCallback consumptionLoadCallback);

  /**
   * Saves the provided Consumption into persistent storage.
   *
   * @param consumption Consumption to be stored.
   * @param consumptionSaveCallback callback used to notify caller about the result of the
   * operation.
   */
  void putConsumption(@NonNull Consumption consumption,
      @NonNull final ConsumptionSaveCallback consumptionSaveCallback);

  /**
   * ConsumptionLoadCallback used to be notified when either a consumption has been loaded or an
   * error happened.
   */
  interface ConsumptionLoadCallback {
    void onConsumptionLoaded(@NonNull final Consumption consumption);

    void onError(@NonNull final ErrorBundle errorBundle);
  }

  /**
   * ConsumptionSaveCallback used to be notified when either a consumption has been saved or an
   * error happened.
   */
  interface ConsumptionSaveCallback {
    void onConsumptionSaved();

    void onError(@NonNull final ErrorBundle errorBundle);
  }
}
