/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain;

/**
 * Consumable interface represents edible objects that can be {@link Consumed} and therefore added
 * to the {@link Consumption}. Example of consumables are meals, beverages, supplements, and
 * prescription drugs.
 */
public interface Consumable {

  /**
   * Returns the name of the consumable
   *
   * @return The name of the consumable
   */
  String getName();

  /**
   * Sets name of the consumable
   *
   * @param name The new for the consumable
   */
  void setName(String name);

  /**
   * Returns consumable in a string format.
   *
   * @return string representation of the  consumable
   */
  String toString();

  /**
   * Returns the duration after this consumable is safe to be consumed after the specified another
   * consumable. As an example, an iron supplement should be consumed at least 120 minutes after a
   * meal.
   *
   * @return the duration in minutes.
   */
  int getSafeToConsumeAfterMinutes(Consumable consumable);
}
