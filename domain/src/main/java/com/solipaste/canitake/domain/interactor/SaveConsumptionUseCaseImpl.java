/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.domain.interactor;

import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import com.solipaste.canitake.domain.repository.ConsumptionRepository;
import edu.umd.cs.findbugs.annotations.NonNull;
import javax.inject.Inject;

/**
 * Concrete implementation of SaveConsumption use case in synchronous manner.
 *
 * TODO: Refactor implementation to perform operation asynchronously.
 */
public class SaveConsumptionUseCaseImpl implements SaveConsumptionUseCase {
  @NonNull private final ConsumptionRepository consumptionRepository;

  /**
   * Creates a new {@code SaveConsumptionUseCaseImpl} with specified repository.
   *
   * @param consumptionRepository the ConsumptionRepository Used to read meals from.
   */
  @Inject public SaveConsumptionUseCaseImpl(@NonNull ConsumptionRepository consumptionRepository) {
    this.consumptionRepository = consumptionRepository;
  }

  /**
   * Executes Get meals use case synchronously.
   *
   * @param consumption the consumption to be saved.
   * @param callback {@link SaveConsumptionUseCase.SaveConsumptionCallback} used to notify the caller
   */
  @Override public void saveConsumption(@NonNull Consumption consumption,
      final SaveConsumptionCallback callback) {
    consumptionRepository.putConsumption(consumption,
        new ConsumptionRepository.ConsumptionSaveCallback() {
          @Override public void onConsumptionSaved() {
            callback.onConsumptionSaved();
          }

          @Override public void onError(@NonNull ErrorBundle errorBundle) {
            callback.onError(errorBundle);
          }
        });
  }
}
