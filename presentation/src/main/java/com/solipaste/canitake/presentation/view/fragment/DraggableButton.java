/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.view.fragment;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.AppCompatButton;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import com.solipaste.canitake.R;

/**
 * {@code DraggableButton} is a button that can be dragged down and back up.
 * Buttons have two line of text to be shown.
 *
 */
public class DraggableButton extends AppCompatButton {

  /**
   * Interface that needs to be implemented by the callback attached to draggable buttons.
   */
  public interface DraggableButtonCallback {
    /**
     * User started scrolling (dragging) the button
     */
    void scrollStarted();

    /**
     * User stopped the scrolling gestures.
     */
    void scrollEnded();

    /**
     * Button was moved to the bottom position
     *
     * @param draggableButton button that was moved.
     */
    void onBottomPosition(DraggableButton draggableButton);

    /**
     * Button was moved to the top position
     *
     * @param draggableButton button that was moved.
     */
    void onTopPosition(DraggableButton draggableButton);

    /**
     * Button was long pressed..
     *
     * @param draggableButton button that was touched.
     */
    void onLongPress(DraggableButton draggableButton);
  }


  private static final String TAG = "DraggableButton";

  private enum Position {TOP, BOTTOM}

  private static final int INVALID_POINTER_ID = -1;
  private static final int ANIMATION_DURATION = 300;

  private static float bottomPosY = 500;

  private DraggableButtonCallback callback;

  private String exampleString = "Medication"; // TODO: use a default from R.string...
  private int exampleColor = Color.RED; // TODO: use a default from R.color...
  private float exampleDimension = 0; // TODO: use a default from R.dimen...
  private Drawable exampleDrawable;
  private String topText;
  private String bottomText;
  private Position position = Position.TOP;

  private TextPaint textPaint;
  private float textWidth;
  private float textHeight;

  private int activePointerId = INVALID_POINTER_ID;
  private int paddingLeft;
  private int paddingTop;
  private int paddingRight;
  private int paddingBottom;
  private int contentWidth;
  private int contentHeight;
  private GestureDetectorCompat gestureDetector;
  private float buttonDownRaw;
  private float buttonDownTranslation;
  private int topAbsolute;

  public DraggableButton(Context context) {
    super(context);
    init(null, 0);
  }

  public DraggableButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(attrs, 0);
  }

  public DraggableButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(attrs, defStyle);
  }

  private void init(AttributeSet attrs, int defStyle) {
    // Load attributes
    final TypedArray a =
        getContext().obtainStyledAttributes(attrs, R.styleable.DraggableButton, defStyle, 0);

    exampleColor = a.getColor(R.styleable.DraggableButton_exampleColor, exampleColor);
    // Use getDimensionPixelSize or getDimensionPixelOffset when dealing with
    // values that should fall on pixel boundaries.
    exampleDimension =
        a.getDimension(R.styleable.DraggableButton_exampleDimension, exampleDimension);

    if (a.hasValue(R.styleable.DraggableButton_exampleDrawable)) {
      exampleDrawable = a.getDrawable(R.styleable.DraggableButton_exampleDrawable);
      exampleDrawable.setCallback(this);
    }

    a.recycle();

    // Set up a default TextPaint object
    textPaint = new TextPaint();
    textPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
    textPaint.setTextAlign(Paint.Align.LEFT);

    // Update TextPaint and text measurements from attributes
    invalidateTextPaintAndMeasurements();

    gestureDetector = new GestureDetectorCompat(getContext(), new DraggableGestureListener());
  }

  private void invalidateTextPaintAndMeasurements() {
    textPaint.setTextSize(exampleDimension);
    textPaint.setColor(exampleColor);
    textWidth = textPaint.measureText(exampleString);

    Paint.FontMetrics fontMetrics = textPaint.getFontMetrics();
    textHeight = fontMetrics.bottom;
  }

  @Override protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
    super.onLayout(changed, left, top, right, bottom);

    paddingLeft = getPaddingLeft();
    paddingTop = getPaddingTop();
    paddingRight = getPaddingRight();
    paddingBottom = getPaddingBottom();

    contentWidth = getWidth() - paddingLeft - paddingRight;
    contentHeight = getHeight() - paddingTop - paddingBottom;

    int[] location = new int[2];
    getLocationOnScreen(location);
    topAbsolute = location[1];
  }

  /**
   * Sets the relative bottom coordinate where the dragged button will stop.
   *
   * @param bottomPosY relative bottom position for the button.
   */
  public static void setBottomPosY(float bottomPosY) {
    DraggableButton.bottomPosY = bottomPosY;
  }

  /**
   * Sets {@link DraggableButtonCallback} to be informed about the user operations.
   *
   * @param callback callback implementing {@link DraggableButtonCallback} interface.
   */
  public void setCallback(DraggableButtonCallback callback) {
    this.callback = callback;
  }

  /**
   * Sets the top text line of the button.
   *
   * @param text text to be shown.
   */
  public void setTopText(CharSequence text) {
    topText = text.toString();
    updateText();
  }

  /**
   * Sets the bottom text line of the button.
   * @param text
   */
  public void setBottomText(CharSequence text) {
    bottomText = text.toString();
    updateText();
  }

  /**
   * Moves button to position relative to the parent view.
   *
   * @param targetPosRelativeToParent coordinate to where move button.
   */
  public void setPositionRelativeToParent(float targetPosRelativeToParent) {
    float targetPos = targetPosRelativeToParent - getTop();
    setPosition(targetPos);
  }

  /**
   * Moves buttons to the final position as a result of a scroll or fling gesture.
   *
   * @param currentPosRelativeToParent the current position of the button.
   */
  public void setPositionRelativeToParentOnRelease(float currentPosRelativeToParent) {
    float targetPos = currentPosRelativeToParent - topAbsolute;
    if (targetPos > bottomPosY / 2) {
      animateToBottomPosition();
    } else {
      animateToTopPosition();
    }
  }

  @SuppressLint("SetTextI18n") private void updateText() {
    setText(topText + '\n' + bottomText);
  }

  private void setPosition(float targetPos) {

    // cap at the top and bottom
    setTranslationY(Math.min(bottomPosY, Math.max(0, targetPos)));
  }

  private void setPositionOnRelease(float targetPos) {
    if (targetPos > bottomPosY / 2) {
      animateToBottomPosition();
    } else {
      animateToTopPosition();
    }
  }

  private void animateToTopPosition() {
    Position oldPosition = position;
    position = Position.TOP;

    // animate and callback only if movement is needed
    if (getTranslationY() != 0) {
      ObjectAnimator animator = ObjectAnimator.ofFloat(this, "translationY", 0);
      animator.setDuration(ANIMATION_DURATION);
      animator.setInterpolator(new FastOutSlowInInterpolator());
      animator.start();

      if (oldPosition != position) {
        callback.onTopPosition(this);
      }
    }
  }

  private void animateToBottomPosition() {
    Position oldPosition = position;
    position = Position.BOTTOM;

    // animate and callback only if movement is needed
    if (getTranslationY() != bottomPosY) {
      ObjectAnimator animator = ObjectAnimator.ofFloat(this, "translationY", bottomPosY);
      animator.setDuration(ANIMATION_DURATION);
      animator.start();
      if (oldPosition != position) {
        callback.onBottomPosition(this);
      }
    }
  }

  /**
   * Together with gesture listener manages all allowed gestures.
   *
   * @param event event received
   * @return {©code true} if event was handled, {©code false} otherwise.
   */
  @Override public boolean onTouchEvent(@NonNull MotionEvent event) {

    final int action = MotionEventCompat.getActionMasked(event);
    int activePointerId = MotionEventCompat.getPointerId(event, 0);

    // Check for additional pointers
    if (this.activePointerId == INVALID_POINTER_ID || activePointerId == this.activePointerId) {

      // remember the id of the first pointer and check it's index
      this.activePointerId = activePointerId;

      // ignore unknown pointers
      if (event.findPointerIndex(this.activePointerId) != -1) {

        switch (action) {

          // we need to store raw Y position during down event
          // in order to calculate scroll movement
          case MotionEvent.ACTION_DOWN: {
            Log.d(TAG, "ACTION_DOWN: " + event);
            callback.scrollStarted();
            // Save the ID of this pointer (for dragging)
            buttonDownRaw = event.getRawY();
            buttonDownTranslation = getTranslationY();
            break;
          }

          // cancel will also stop all gestures
          case MotionEvent.ACTION_CANCEL: {
            Log.d(TAG, "ACTION_CANCEL: " + event);
            setPositionRelativeToParentOnRelease(event.getRawY());
            this.activePointerId = INVALID_POINTER_ID;
            callback.scrollEnded();
            break;
          }

          default: {
            // no special handling for other events
            break;
          }
        }

        // check for any gestures
        boolean gestureWasDetected = this.gestureDetector.onTouchEvent(event);

        // ignore up if gesture detector detected a gesture
        if (!gestureWasDetected && action == MotionEvent.ACTION_UP) {
          Log.d(TAG, "ACTION_UP without gestures: " + event);
          setPositionRelativeToParentOnRelease(event.getRawY());
          this.activePointerId = INVALID_POINTER_ID;
          callback.scrollEnded();
        }

        // ensure that button specific gestures are handled
        super.onTouchEvent(event);
      } else { // no event data for the pointer with active ID
        Log.d(TAG, "onTouchEvent: no data for pointerID: " + this.activePointerId);
        return false;
      }
    } else { // all events for other pointers (fingers) ignored
      return false;
    }

    // if we end up here then event was handled successfully
    return true;
  }

  class DraggableGestureListener extends GestureDetector.SimpleOnGestureListener {
    private static final String TAG = "Gestures";
    private int MIN_FLING_VELOCITY = 300;

    public DraggableGestureListener() {
      super();
      final ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
      MIN_FLING_VELOCITY = viewConfiguration.getScaledPagingTouchSlop();
    }

    @Override public boolean onDown(MotionEvent e) {
      return true;
    }

    @Override public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX,
        float velocityY) {
      Log.d(TAG, "onFling vY: " + velocityY);

      if (velocityY > 0) { // down
        if (velocityY > MIN_FLING_VELOCITY) {
          animateToBottomPosition();
        } else {
          animateToTopPosition();
        }
      } else if (velocityY < 0) { // up
        if (velocityY < -MIN_FLING_VELOCITY) {
          animateToTopPosition();
        } else {
          animateToBottomPosition();
        }
      }
      return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
      float targetPos = buttonDownTranslation + e2.getRawY() - buttonDownRaw;
      Log.d(TAG, "onScroll to " + targetPos);
      setPosition(targetPos);
      return true;
    }

    @Override public boolean onSingleTapUp(MotionEvent e) {
      Log.d(TAG, "onSingleTapUp");
      // move to opposite position
      if (getTranslationY() > 0) {
        animateToTopPosition();
      } else {
        animateToBottomPosition();
      }
      return true;
    }

    @Override public void onShowPress(MotionEvent e) {
      Log.d(TAG, "onShowPress");
      super.onShowPress(e);
    }

    @Override public void onLongPress(MotionEvent e) {
      super.onLongPress(e);
      callback.onLongPress(DraggableButton.this);
    }
  }
}
