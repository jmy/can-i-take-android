/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.view.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.solipaste.canitake.R;
import com.solipaste.canitake.presentation.view.fragment.ConsumptionFragment;

/**
 * {@code MainActivity} of the Android application. Shows a single {@link ConsumptionFragment}
 * and means to create new consumables.
 *
 */
public class MainActivity extends BaseActivity {
  private static final String TAG = "MainActivity";
  private static final int RESULT_CODE_FROM_HISTORY_ACTIVITY = 0;

  @Bind(R.id.scrollView) HorizontalScrollView horizontalScrollView;

  @Bind(R.id.addButton) FloatingActionButton addButton;
  private ConsumptionFragment consumptionFragment;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    consumptionFragment =
        (ConsumptionFragment) getSupportFragmentManager().findFragmentById(R.id.button_list);
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_CODE_FROM_HISTORY_ACTIVITY) {
      // disable animations if returning from the sub-activity
      overridePendingTransition(0, 0);
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    switch (id) {
      case R.id.action_settings:
        return true;
      case R.id.action_history:
        startHistoryActivity();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  /**
   * Launches a new {@link HistoryActivity}.
   */
  private void startHistoryActivity() {
    final Intent intent = new Intent(this, HistoryActivity.class);
    if (Build.VERSION.SDK_INT >= 16) {
      View menuItemView = findViewById(R.id.action_history);
      Bundle activityOptionsBundle =
          ActivityOptionsCompat.makeScaleUpAnimation(menuItemView, 0, 0, menuItemView.getWidth(),
              menuItemView.getHeight()).toBundle();
      startActivityForResult(intent, RESULT_CODE_FROM_HISTORY_ACTIVITY, activityOptionsBundle);
    } else {
      startActivityForResult(intent, RESULT_CODE_FROM_HISTORY_ACTIVITY);
      overridePendingTransition(R.anim.slide_in_bottom, 0);
    }
  }

  /**
   * Add button clicked. Ask fragment to create a new consumable.
   * @param button
   */
  @OnClick(R.id.addButton) void onClick(FloatingActionButton button) {
    Log.d(TAG, "onAddButtonClicked() called with button = [" + button + ']');
    consumptionFragment.createConsumableWithNewName();
  }
}
