/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.view;

import android.app.Application;
import com.solipaste.canitake.presentation.di.HasComponent;
import com.solipaste.canitake.presentation.di.components.ApplicationComponent;
import com.solipaste.canitake.presentation.di.components.DaggerApplicationComponent;
import com.solipaste.canitake.presentation.di.modules.ApplicationModule;

/**
 * {@code AndroidApplication} class that initializes all dependencies in {@link
 * ApplicationComponent}.
 */
public class AndroidApplication extends Application implements HasComponent<ApplicationComponent> {

  @Override public void onCreate() {
    super.onCreate();
    Injector.INSTANCE.initializeApplicationComponent(this);
  }

  /**
   * Returns {@code ApplicationComponent}.
   *
   * @return a Dagger {@code ApplicationComponent}
   */
  @Override public ApplicationComponent getComponent() {
    return Injector.INSTANCE.getApplicationComponent();
  }

  /**
   * Singleton storing the created {@code ApplicationComponent} instance.
   */
  public enum Injector {
    INSTANCE;

    private ApplicationComponent applicationComponent;

    public ApplicationComponent getApplicationComponent() {
      return applicationComponent;
    }

    void initializeApplicationComponent(AndroidApplication androidApplication) {
      this.applicationComponent = DaggerApplicationComponent.builder()
          .applicationModule(new ApplicationModule(androidApplication))
          .build();
    }
  }
}
