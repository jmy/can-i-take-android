/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.solipaste.canitake.R;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.presentation.di.components.ApplicationComponent;
import com.solipaste.canitake.presentation.presenter.HistoryPresenter;
import com.solipaste.canitake.presentation.view.HistoryListLayoutManager;
import com.solipaste.canitake.presentation.view.HistoryView;
import com.solipaste.canitake.presentation.view.adapter.HistoryAdapter;
import javax.inject.Inject;

/**
 * {@code HistoryFragment} used to show the full list of all consumed items.
 */
public class HistoryFragment extends BaseFragment implements HistoryView {
  private static final String TAG = "HistoryFragment";

  @Inject HistoryPresenter historyPresenter;

  @Bind(R.id.historyRecyclerView) RecyclerView historyRecyclerView;

  @Override public void onAttach(Context context) {
    super.onAttach(context);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    final ViewGroup rootView =
        (ViewGroup) inflater.inflate(R.layout.fragment_history, container, true);
    ButterKnife.bind(this, rootView);

    final HistoryListLayoutManager historyLayoutManager =
        new HistoryListLayoutManager(getContext());
    historyRecyclerView.setLayoutManager(historyLayoutManager);
    return rootView;
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    initialize();
    loadHistory();
  }

  @Override public void onResume() {
    super.onResume();
    historyPresenter.resume();
  }

  @Override public void onPause() {
    super.onPause();
    historyPresenter.pause();
  }

  @Override public void onDestroy() {
    super.onDestroy();
    historyPresenter.destroy();
  }

  /**
   * Renders all the consumed items in the specified Consumption in the view using RecyclerView.
   *
   * @param consumption {@code Consumption} to be rendered.
   */
  @Override public void renderHistory(Consumption consumption) {
    final HistoryAdapter historyAdapter = new HistoryAdapter(getContext(), consumption);
    historyRecyclerView.setAdapter(historyAdapter);
  }

  /**
   * Shows user positive user feedback using toast.
   *
   * @param message text message to be shown.
   */
  @Override public void showSuccess(String message) {
    showToastMessage(message);
  }

  /**
   * Shows user negative user feedback using toast.
   *
   * @param errorMessage text message to be shown.
   */
  @Override public void showError(String errorMessage) {
    showToastMessage(errorMessage);
  }

  /**
   * Initializes injections and links presenter to this view.
   */
  private void initialize() {
    getComponent(ApplicationComponent.class).inject(this);
    historyPresenter.setView(this);
  }

  /**
   * Loads history using presenter.
   */
  private void loadHistory() {
    historyPresenter.initialize();
  }
}

