/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.view.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import butterknife.ButterKnife;
import com.solipaste.canitake.R;
import com.solipaste.canitake.presentation.di.components.ApplicationComponent;
import com.solipaste.canitake.presentation.view.fragment.HistoryFragment;

/**
 * {@code HistoryActivity} consisting of a single {@link HistoryActivity}.
 */
public class HistoryActivity extends BaseActivity {
  private static final String TAG = "HistoryActivity";

  private HistoryFragment historyFragment;

  private ApplicationComponent applicationComponent;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_history);
    ButterKnife.bind(this);

    historyFragment =
        (HistoryFragment) getSupportFragmentManager().findFragmentById(R.id.historyListFragment);

    if (getSupportActionBar() != null) {
      getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_history, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public void finish() {
    super.finish();
    // custom finish animation with transparent background
    overridePendingTransition(0, R.anim.slide_out_down);
  }
}

