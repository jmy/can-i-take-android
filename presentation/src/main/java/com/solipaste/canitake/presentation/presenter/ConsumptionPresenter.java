/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.presenter;

import android.annotation.Nullable;
import android.util.Log;
import com.solipaste.canitake.domain.Consumable;
import com.solipaste.canitake.domain.Consumed;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.Meal;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import com.solipaste.canitake.domain.interactor.GetConsumptionUseCase;
import com.solipaste.canitake.domain.interactor.SaveConsumptionUseCase;
import com.solipaste.canitake.domain.interactor.TakeMealUseCase;
import com.solipaste.canitake.presentation.presenter.exception.ErrorMessageFactory;
import com.solipaste.canitake.presentation.view.ConsumptionView;
import edu.umd.cs.findbugs.annotations.NonNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.inject.Inject;

/**
 * Concrete {@code ConsumptionPresenter}. Functions as a glue between {@code
 * ConsumptionFragment} via ConsumptionView and domain modules.
 *
 * The current passes domain classes from domain module directly to the view. It should be
 * considered if the view should use view specific domain objects that are mapped to and from by
 * the presenter.
 */
public class ConsumptionPresenter implements Presenter {
  private static final String TAG = "ConsumptionPresenter";

  @NonNull private final GetConsumptionUseCase getConsumptionUseCase;
  @NonNull private final TakeMealUseCase takeMealUseCase;
  @NonNull private final SaveConsumptionUseCase saveConsumptionUseCase;
  @NonNull private final DateFormat consumedTimeFormat;

  @Nullable private ConsumptionView consumptionView;

  /**
   * Constructs {@code ConsumptionPresenter} with given arguments.
   *
   * @param getConsumptionUseCase {@code GetConsumptionUseCase} implementation.
   * @param saveConsumptionUseCase {@code SaveConsumptionUseCase} implementation.
   * @param takeMealUseCase {@code TakeMealUseCase} implementation.
   */
  @Inject public ConsumptionPresenter(GetConsumptionUseCase getConsumptionUseCase,
      SaveConsumptionUseCase saveConsumptionUseCase, TakeMealUseCase takeMealUseCase) {
    this.getConsumptionUseCase = getConsumptionUseCase;
    this.saveConsumptionUseCase = saveConsumptionUseCase;
    this.takeMealUseCase = takeMealUseCase;

    consumedTimeFormat = SimpleDateFormat.getTimeInstance();
  }

  /**
   * Sets the view used by this presenter. Must be called when the view is initialized
   */
  public void setView(@NonNull ConsumptionView consumptionView) {
    this.consumptionView = consumptionView;
  }

  /**
   * Gets the view used by this presenter.
   */
  public ConsumptionView getConsumptionView() {
    return consumptionView;
  }

  @Override public void resume() {
    Log.d(TAG, "resume ");
  }

  @Override public void pause() {
    Log.d(TAG, "pause ");
  }

  @Override public void destroy() {
    this.consumptionView = null;
    Log.d(TAG, "destroy ");
  }

  /**
   * Initializes {@code ConsumptionPresenter}.
   * <p>
   * ConsumptionPresenter.setView must have been called before this.
   */
  public void initialize() {
    if (consumptionView == null) {
      throw new IllegalStateException("Called initialize before view was set.");
    }

    hideViewRetry();
    showViewLoading();
    getConsumption();
  }

  /**
   * Shows error message in view.
   *
   * @param errorBundle {@code ErrorBundle} containing the error.
   */
  private void showErrorMessage(ErrorBundle errorBundle) {
    if (consumptionView == null) {
      Log.w(TAG, "No view set. Ignoring error: " + errorBundle.getErrorMessage());
      return;
    }

    String errorMessage =
        ErrorMessageFactory.create(consumptionView.getContext(), errorBundle.getException());
    consumptionView.showError(errorMessage);
  }

  private void showViewRetry() {
    // TODO: 15.9.15
  }

  /**
   * Gets consumption from the backend.
   */
  private void getConsumption() {
    if (consumptionView == null) {
      throw new IllegalStateException("View not set.");
    }

    getConsumptionUseCase.getConsumption(new GetConsumptionUseCase.GetConsumptionCallback() {
      @Override public void onConsumptionLoaded(Consumption consumption) {
        showConsumptionInView(consumption);
      }

      @Override public void onError(@NonNull ErrorBundle errorBundle) {
        showErrorMessage(errorBundle);
        showViewRetry();
      }
    });
  }

  /**
   * Shows consumption in view.
   *
   * @param consumption {@code Consumption} to be shown.
   */
  private void showConsumptionInView(Consumption consumption) {
    if (consumptionView == null) {
      throw new IllegalStateException("View not set.");
    }

    consumptionView.renderConsumption(consumption);
  }

  /**
   * Not implemented.
   * TODO: implement progress.
   */
  private void showViewLoading() {
    // TODO: 15.9.15
  }

  /**
   * Not implemented.
   * TODO: implement retry.
   */
  private void hideViewRetry() {
    // TODO: 15.9.15
  }

  /**
   * Marks meal as consumed. Updates the backend.
   *
   * @param meal the meal consumed.
   */
  public void takeMeal(final Meal meal) {
    if (consumptionView == null) {
      throw new IllegalStateException("View not set.");
    }

    takeMealUseCase.takeMeal(meal, new TakeMealUseCase.TakeMealCallback() {
      @Override public void onMealTaken(@NonNull Consumed consumed) {

        if (consumptionView != null) {
          final String timeString = consumedTimeFormat.format(consumed.getTime().getTime());
          consumptionView.showSuccess("Meal taken at " + timeString);
          consumptionView.updateMealTakeTime(meal, timeString);
        } else {
          Log.w(TAG, "No view set. Ignoring the results.");
        }
      }

      @Override public void onError(@NonNull ErrorBundle errorBundle) {
        if (consumptionView != null) {
          showErrorMessage(errorBundle);
          consumptionView.cancelMealTake(meal);
          showViewRetry();
        } else {
          Log.w(TAG, "No view set. Ignoring error: " + errorBundle.getErrorMessage());
        }
      }
    });
  }

  /**
   * Saves consumable with new content in some persistent manner. Called when user has modified
   * consumable.
   *
   * TODO: Need to consider if this should be allowed and whether it should also affect other
   * consumables of same type.
   *
   * @param consumable to
   */
  public void saveConsumable(Consumable consumable) {
    //        saveConsumptionUseCase.saveConsumption(consumption, new SaveConsumptionUseCase.SaveConsumptionCallback() {
    //            @Override
    //            public void onConsumptionSaved() {
    //                consumptionView.showSuccess("Consumption saved.");
    //            }
    //
    //            @Override
    //            public void onError(@NonNull ErrorBundle errorBundle) {
    //                showErrorMessage(errorBundle);
    //                showViewRetry();
    //            }
    //        });
  }
}
