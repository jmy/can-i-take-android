/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.view;

import android.content.Context;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.Meal;

/**
 * Interface representing Consumption specific view in MVP pattern. Used by presenters
 * communicate with user interface.
 */
public interface ConsumptionView {

  /**
   * Renders consumption into user interface.
   *
   * @param consumption {@code Consumption} to be rendered.
   */
  void renderConsumption(Consumption consumption);

  /**
   * Shows an error message in user interface.
   *
   * @param errorMessage error text to be shown.
   */
  void showError(String errorMessage);

  /**
   * Gets context from Android user interface.
   *
   * @return {@code Context} attached to user interface
   */
  Context getContext();

  /**
   * Instructs user interface to cancel consumption of the meal.
   *
   * @param meal {@code Meal} to be "unconsumed".
   */
  void cancelMealTake(Meal meal);

  /**
   * Show a short success message in user interface.
   *
   * @param message message to be shown
   */
  void showSuccess(String message);

  /**
   * Update the time when the specified meal was consumed.
   *
   * @param meal {@code Meal} that needs to be updated.
   * @param timeString formatted time string to be shown in user interface.
   */
  void updateMealTakeTime(Meal meal, String timeString);
}
