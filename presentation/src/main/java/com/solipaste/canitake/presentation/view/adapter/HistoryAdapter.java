/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.solipaste.canitake.R;
import com.solipaste.canitake.domain.Consumed;
import com.solipaste.canitake.domain.Consumption;
import java.text.DateFormat;
import java.util.Date;

/**
 * {@code HistoryAdapter} show all consumed items in the consumption provided to it during
 * construction.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {
  private final Consumption consumption;
  private final LayoutInflater layoutInflater;
  private final java.text.DateFormat dateTimeFormat;

  /**
   * Constructs new {@code HistoryAdapter}.
   *
   * @param context context
   * @param consumption {@code Consumption} that contains items to show
   */
  public HistoryAdapter(Context context, Consumption consumption) {

    this.layoutInflater =
        (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    this.consumption = consumption;

    dateTimeFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
  }

  /**
   * Creates layout from XML
   *
   * @param parent The ViewGroup into which the new View will be added after it is bound to an
   * adapter position.
   * @param viewType The view type of the new View. Only one type supported.
   * @return A new ViewHolder that holds the view
   */
  @Override public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    final View view = layoutInflater.inflate(R.layout.row_cosumed, parent, false);
    final HistoryViewHolder holder = new HistoryViewHolder(view);
    return holder;
  }

  /**
   * Binds time and name of the consumed item in the specified position to the specified ViewHolder
   *
   * @param holder holder to update with the data
   * @param position position of the consumed item in the consumptions
   */
  @Override public void onBindViewHolder(HistoryViewHolder holder, int position) {
    final Consumed consumed = consumption.getConsumed().get(position);

    final Date time = consumed.getTime().getTime();
    holder.textViewTime.setText(dateTimeFormat.format(time));
    holder.textViewName.setText(consumed.getConsumable().getName());
  }

  @Override public int getItemCount() {
    return consumption.getConsumed().size();
  }

  /**
   * A ViewHolder containing the two fields defined in the layout.
   */
  static class HistoryViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.row_consumed_time) TextView textViewTime;
    @Bind(R.id.row_consumed_name) TextView textViewName;

    public HistoryViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
