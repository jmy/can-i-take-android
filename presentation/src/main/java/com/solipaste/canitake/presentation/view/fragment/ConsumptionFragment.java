/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.solipaste.canitake.R;
import com.solipaste.canitake.domain.Consumable;
import com.solipaste.canitake.domain.Consumed;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.Meal;
import com.solipaste.canitake.domain.repository.ConsumptionRepository;
import com.solipaste.canitake.presentation.di.components.ApplicationComponent;
import com.solipaste.canitake.presentation.presenter.ConsumptionPresenter;
import com.solipaste.canitake.presentation.view.ConsumptionView;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import javax.inject.Inject;

/**
 * A consumption fragment showing {@code Consumption} in Android user interface.
 * Shows {@link Consumable}s as {@link DraggableButton}s ordered from left to right. Allows user
 * to mark consumable as consumed by dragging to towards the bottom of view.
 *
 */
public class ConsumptionFragment extends BaseFragment
    implements DraggableButton.DraggableButtonCallback, ConsumptionView {
  private static final String TAG = "ConsumptionFragment";

  public static final int BOTTOM_MARGIN = 120;

  @Inject ConsumptionPresenter consumptionPresenter;
  @Inject ConsumptionRepository consumptionRepository;

  DateFormat consumedTimeFormat;
  private ViewGroup rootView;
  private Map<Consumable, DraggableButton> consumableMap = new LinkedHashMap<>();

  @Override public void onAttach(Context context) {
    super.onAttach(context);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    rootView = (ViewGroup) inflater.inflate(R.layout.fragment_button_list, container, true);

    // calculate the bottom position for buttons
    rootView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {

      @Override
      public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft,
          int oldTop, int oldRight, int oldBottom) {
        DraggableButton.setBottomPosY(bottom - BOTTOM_MARGIN);
        Log.d(TAG, "onLayoutChange");
      }
    });

    consumedTimeFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());

    return rootView;
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    initialize();
    loadConsumption();
  }

  @Override public void onResume() {
    super.onResume();
    consumptionPresenter.resume();
  }

  @Override public void onPause() {
    super.onPause();
    consumptionPresenter.pause();
  }

  @Override public void onDestroy() {
    super.onDestroy();
    consumptionPresenter.destroy();
  }

  @Override public void renderConsumption(Consumption consumption) {
    removeAllConsumables();
    for (Consumed consumed : consumption.getConsumed()) {
      addConsumedToView(consumed);
    }

    // make sure that there is at least one meal always
    if (consumableMap.isEmpty()) {
      addConsumableToView(new Meal("meal"), null);
    }
  }

  /**
   * Shows {@code Consumed} in view.
   *
   * @param consumed consumed to be shown.
   */
  private void addConsumedToView(Consumed consumed) {
    Consumable consumable = consumed.getConsumable();
    addConsumableToView(consumable, consumed.getTime());
  }

  /**
   * Cancel meal consumption in view.
   *
   * @param meal {@code Meal} to be "unconsumed".
   */
  @Override public void cancelMealTake(Meal meal) {
    DraggableButton button = consumableMap.get(meal);
    if (button != null) {
      button.setPositionRelativeToParent(0);
    }
    showToastMessage("Meal cancelled");
  }

  /**
   * Shows success toast.
   *
   * @param message message to be shown
   */
  @Override public void showSuccess(String message) {
    showToastMessage(message);
  }

  /**
   * Updates the time meal was taken.
   *
   * @param meal {@code Meal} that needs to be updated.
   * @param timeString formatted time string to be shown in user interface.
   */
  @Override public void updateMealTakeTime(Meal meal, String timeString) {
    for (Map.Entry<Consumable, DraggableButton> entry : consumableMap.entrySet()) {
      if (entry.getKey() == meal) {
        entry.getValue().setBottomText(timeString);
        break;
      }
    }
  }

  /**
   * Shows error toast.
   *
   * @param errorMessage error text to be shown.
   */
  @Override public void showError(String errorMessage) {
    showToastMessage(errorMessage);
  }

  /**
   * User started scrolling the fragment content.
   */
  @Override public void scrollStarted() {
    rootView.requestDisallowInterceptTouchEvent(true);
  }

  /**
   * User stopped scrolling the fragment content.
   */
  @Override public void scrollEnded() {
    rootView.requestDisallowInterceptTouchEvent(false);
  }

  /**
   * Button was moved to the bottom position
   *
   * @param draggableButton button that was moved.
   */
  @Override public void onBottomPosition(DraggableButton draggableButton) {
    Log.d(TAG, "onBottomPosition ");
    for (Map.Entry<Consumable, DraggableButton> entry : consumableMap.entrySet()) {
      if (entry.getValue() == draggableButton && entry.getKey() instanceof Meal) {
        takeMeal((Meal) entry.getKey());
      }
    }
  }

  /**
   * Button was moved to the top position
   *
   * @param draggableButton button that was moved.
   */
  @Override public void onTopPosition(DraggableButton draggableButton) {
    Log.d(TAG, "onTopPosition ");
    for (Map.Entry<Consumable, DraggableButton> entry : consumableMap.entrySet()) {
      if (entry.getValue() == draggableButton && entry.getKey() instanceof Meal) {
        cancelMealTake((Meal) entry.getKey());
        // TODO: 17.9.15 Update the model also
      }
    }
  }

  /**
   * Button was long pressed in order to activate editing.
   *
   * @param draggableButton button that was touched.
   */
  @Override public void onLongPress(DraggableButton draggableButton) {
    for (Map.Entry<Consumable, DraggableButton> entry : consumableMap.entrySet()) {
      if (entry.getValue() == draggableButton) {
        editConsumableName(entry.getKey());
        break;
      }
    }
  }

  /**
   * Creation of a new consumable was created. Currently always creates a {@code Meal}.
   * TODO Allow user to alternatively create other {@code Consumables} also.
   */
  public void createConsumableWithNewName() {
    final EditText input = new EditText(getContext());
    DialogInterface.OnClickListener successListener = new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int whichButton) {
        addConsumableToView(new Meal(input.getText().toString()), null);
      }
    };
    showTextEditorDialog(input, successListener);
  }

  /**
   * User requested that she wants to edit the name of the consumable.
   *
   * @param consumable
   */
  public void editConsumableName(final Consumable consumable) {
    final DraggableButton button = consumableMap.get(consumable);
    final EditText input = new EditText(getContext());
    input.setText(consumable.getName());

    DialogInterface.OnClickListener successListener = new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int whichButton) {
        // update name
        consumable.setName(input.getText().toString());
        if (button != null) {
          button.setTopText(consumable.getName());
        }
        consumptionPresenter.saveConsumable(consumable);
      }
    };
    showTextEditorDialog(input, successListener);
  }

  /**
   * Initializes the fragment.
   */
  private void initialize() {
    getComponent(ApplicationComponent.class).inject(this);
    consumptionPresenter.setView(this);
  }

  /**
   * Removes all buttons from the view.
   */
  private void removeAllConsumables() {
    consumableMap.clear();
    rootView.removeAllViews();
  }

  private void loadConsumption() {
    consumptionPresenter.initialize();
  }

  private void addConsumableToView(Consumable consumable, @Nullable Calendar time) {
    LayoutInflater mInflater =
        (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    DraggableButton newButton =
        (DraggableButton) mInflater.inflate(R.layout.draggable_button, rootView, false);
    newButton.setTopText(consumable.getName());
    if (time != null) {
      newButton.setBottomText(consumedTimeFormat.format(time.getTime()));
    } else {
      newButton.setBottomText("");
    }
    newButton.setCallback(this);
    consumableMap.put(consumable, newButton);
    rootView.addView(newButton);
  }

  /**
   * Informs presenter that the specified meal should be consumed.
   *
   * @param meal {@code Meal} to be consumed.
   */
  private void takeMeal(Meal meal) {
    consumptionPresenter.takeMeal(meal);
  }

  /**
   * Show text editor for setting new name for consumable.
   * @param editTextInput the current name
   * @param successListener the listener to be called when Ok is pressed.
   */
  private void showTextEditorDialog(EditText editTextInput,
      DialogInterface.OnClickListener successListener) {
    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

    alert.setTitle("Consumable Name");
    alert.setMessage("Set name");
    alert.setView(editTextInput);

    alert.setPositiveButton("Ok", successListener);

    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int whichButton) {
        // Canceled.
      }
    });

    alert.show();
  }
}
