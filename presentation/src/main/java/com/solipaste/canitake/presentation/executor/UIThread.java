/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.executor;

import android.os.Handler;
import android.os.Looper;
import com.solipaste.canitake.domain.interactor.PostExecutionThread;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Concrete class implementing {@link PostExecutionThread} used to run use case callbacks in Main
 * thread on Android devices.
 */
@Singleton
public class UIThread implements PostExecutionThread {

  private Handler handler;

  /**
   * Constructs {@code UIThread}.
   */
  @Inject
  public UIThread() {
    handler = new Handler(Looper.getMainLooper());
  }

  /**
   * Posts runnable to {@code Handler} for running in Main thread.
   * @param runnable
   */
  @Override public void post(Runnable runnable) {
    handler.post(runnable);
  }

}
