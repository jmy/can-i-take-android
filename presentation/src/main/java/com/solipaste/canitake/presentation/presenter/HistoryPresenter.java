/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.presenter;

import android.annotation.Nullable;
import android.util.Log;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import com.solipaste.canitake.domain.interactor.GetConsumptionUseCase;
import com.solipaste.canitake.presentation.view.HistoryView;
import edu.umd.cs.findbugs.annotations.NonNull;
import javax.inject.Inject;

/**
 * Concrete presenter class for showing full consumption history.
 * TODO: implement class
 */
public class HistoryPresenter implements Presenter {
  private static final String TAG = "HistoryPresenter";

  @NonNull private final GetConsumptionUseCase getConsumptionUseCase;
  @Nullable private HistoryView historyView;

  @Inject public HistoryPresenter(GetConsumptionUseCase getConsumptionUseCase) {
    this.getConsumptionUseCase = getConsumptionUseCase;
  }

  @Override public void resume() {
    Log.d(TAG, "resume()");
  }

  @Override public void pause() {
    Log.d(TAG, "pause()");
  }

  /**
   * Detach presenter from the view.
   */
  @Override public void destroy() {
    Log.d(TAG, "destroy()");
    historyView = null;
  }

  /**
   * Sets view used by this presenter. Must be set before the presenter is used.
   *
   * @param historyView {@link HistoryView} used by this presenter.
   */
  public void setView(HistoryView historyView) {
    this.historyView = historyView;
  }

  public void initialize() {
      if (historyView == null) {
        throw new IllegalStateException("Called initialize before view was set.");
      }

    getConsumptionUseCase.getConsumption(new GetConsumptionUseCase.GetConsumptionCallback() {
      @Override public void onConsumptionLoaded(Consumption consumption) {
        if (historyView != null) {
          historyView.renderHistory(consumption);
        } else {
          Log.w(TAG, "No view set. Ignoring the results.");
        }
      }

      @Override public void onError(@NonNull ErrorBundle errorBundle) {
        if (historyView != null) {
          historyView.showError(errorBundle.getErrorMessage());
        } else {
          Log.w(TAG, "No view set. Ignoring error: " + errorBundle.getErrorMessage());
        }
      }
    });
  }
}
