/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.presenter.exception;

import android.content.Context;

public class ErrorMessageFactory {
  public static String create(Context context, Exception exception) {
    if (context == null || exception == null) {
      return "!! missing context or exception !!";
    }
    return String.format("Error: <%s>, %s", context.toString(), exception.getLocalizedMessage());
  }
}
