/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.view;

import android.content.Context;
import com.solipaste.canitake.domain.Consumption;

/**
 * Interface presenting history specific View in MVP pattern. Used by presenter to communicate
 * with the user interface.
 */
public interface HistoryView {

  /**
   * Renders consumption in view.
   *
   * @param consumption
   */
  void renderHistory(Consumption consumption);

  /**
   * Shows success message in view.
   *
   * @param message text message to be shown.
   */
  void showSuccess(String message);

  /**
   * Shows error message in view.
   *
   * @param errorMessage error message to be shown.
   */
  void showError(String errorMessage);

  Context getContext();
}
