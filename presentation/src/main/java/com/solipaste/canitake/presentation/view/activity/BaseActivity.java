/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Base class for all Android {@code Activity} classes.
 *
 */
public abstract class BaseActivity extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // TODO: 28.9.15 Add activity specific component and inject it here
    //        this.getApplicationComponent().inject(this);
  }

  //    protected ApplicationComponent getApplicationComponent() {
  //        return ((AndroidApplication)getApplication()).getApplicationComponent();
  //    }

  //    protected ActivityModule getActivityModule() {
  //        return new ActivityModule(this);
  //    }
}
