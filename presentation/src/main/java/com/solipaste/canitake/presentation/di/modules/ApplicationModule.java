/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.di.modules;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.solipaste.canitake.data.cache.ConsumptionCache;
import com.solipaste.canitake.data.cache.ConsumptionCacheImpl;
import com.solipaste.canitake.data.repository.ConsumptionDataRepository;
import com.solipaste.canitake.domain.interactor.GetConsumptionUseCase;
import com.solipaste.canitake.domain.interactor.GetConsumptionUseCaseImpl;
import com.solipaste.canitake.domain.interactor.PostExecutionThread;
import com.solipaste.canitake.domain.interactor.SaveConsumptionUseCase;
import com.solipaste.canitake.domain.interactor.SaveConsumptionUseCaseImpl;
import com.solipaste.canitake.domain.interactor.TakeMealUseCase;
import com.solipaste.canitake.domain.interactor.TakeMealUseCaseImpl;
import com.solipaste.canitake.domain.interactor.UseCaseExecutor;
import com.solipaste.canitake.domain.repository.ConsumptionRepository;
import com.solipaste.canitake.presentation.executor.UIThread;
import com.solipaste.canitake.presentation.executor.UseCaseExecutorImpl;
import com.solipaste.canitake.presentation.view.AndroidApplication;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * {@code ApplicationModules} providing implementations for all application scoped dependencies
 * using Dagger 2 Dependency Injector.
 */
@Module public class ApplicationModule {
  private static final String SETTINGS_FILE_NAME = "com.solipaste.canitake.SETTINGS";
  private final AndroidApplication application;

  public ApplicationModule(AndroidApplication application) {
    this.application = application;
  }

  @Provides @Singleton Context provideApplicationContext() {
    return this.application;
  }

  @Provides @Singleton UseCaseExecutor provideUseCaseExecutor(UseCaseExecutorImpl useCaseExecutor) {
    return useCaseExecutor;
  }

  @Provides @Singleton PostExecutionThread providePostExecutionThread(UIThread uiThread) {
    return uiThread;
  }

  @Provides @Singleton SharedPreferences provideSharedPreferences(Context context) {
    return context.getSharedPreferences(SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
  }

  @Provides @Singleton Gson provideGson() {
    return new Gson();
  }

  @Provides @Singleton ConsumptionRepository provideConsumptionRepository(
      ConsumptionDataRepository consumptionDataRepository) {
    return consumptionDataRepository;
  }

  @Provides ConsumptionCache provideConsumptionCache(ConsumptionCacheImpl consumptionCache) {
    return consumptionCache;
  }

  @Provides GetConsumptionUseCase provideGetConsumptionUseCase(
      GetConsumptionUseCaseImpl getConsumptionUseCase) {
    return getConsumptionUseCase;
  }

  @Provides SaveConsumptionUseCase provideSaveConsumptionUseCase(
      SaveConsumptionUseCaseImpl saveConsumptionUseCase) {
    return saveConsumptionUseCase;
  }

  @Provides TakeMealUseCase provideTakeMealUseCase(TakeMealUseCaseImpl takeMealUseCase) {
    return takeMealUseCase;
  }
}
