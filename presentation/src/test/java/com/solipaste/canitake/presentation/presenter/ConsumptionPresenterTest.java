/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.presentation.presenter;

import android.content.Context;
import com.solipaste.canitake.domain.Consumed;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.Meal;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import com.solipaste.canitake.domain.interactor.GetConsumptionUseCase;
import com.solipaste.canitake.domain.interactor.SaveConsumptionUseCase;
import com.solipaste.canitake.domain.interactor.TakeMealUseCase;
import com.solipaste.canitake.presentation.view.ConsumptionView;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConsumptionPresenterTest {

  @Mock ConsumptionView mockConsumptionView;
  @Mock TakeMealUseCase mockTakeMealUseCase;
  @Mock GetConsumptionUseCase mockGetConsumptionUseCase;
  @Mock SaveConsumptionUseCase saveConsumptionUseCase;
  private ConsumptionPresenter consumptionPresenter;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    consumptionPresenter = new ConsumptionPresenter(mockGetConsumptionUseCase, saveConsumptionUseCase,
        mockTakeMealUseCase);
    consumptionPresenter.setView(mockConsumptionView);
  }

  @Test public void successfulInitializeShouldRenderConsumptionInView() throws Exception {

    // execute
    consumptionPresenter.initialize();

    // setup
    Consumption mockConsumption = mock(Consumption.class);
    ArgumentCaptor<GetConsumptionUseCase.GetConsumptionCallback>
        getConsumptionCallbackArgumentCaptor =
        ArgumentCaptor.forClass(GetConsumptionUseCase.GetConsumptionCallback.class);
    verify(mockGetConsumptionUseCase).getConsumption(
        getConsumptionCallbackArgumentCaptor.capture());

    // call back
    getConsumptionCallbackArgumentCaptor.getValue().onConsumptionLoaded(mockConsumption);

    // verify
    verify(mockConsumptionView).renderConsumption(mockConsumption);
  }

  @Test public void failedInitializeConsumptionShouldShowError() throws Exception {
    // execute
    consumptionPresenter.initialize();

    // setup
    final Context mockContext = mock(Context.class);
    when(mockConsumptionView.getContext()).thenReturn(mockContext);

    ErrorBundle mockErrorBundle = mock(ErrorBundle.class);
    final String errorMessage = "test error message";
    when(mockErrorBundle.getErrorMessage()).thenReturn(errorMessage);
    final Exception mockException = mock(Exception.class);
    when(mockException.getLocalizedMessage()).thenReturn(errorMessage);
    when(mockErrorBundle.getException()).thenReturn(mockException);

    ArgumentCaptor<GetConsumptionUseCase.GetConsumptionCallback>
        getConsumptionCallbackArgumentCaptor =
        ArgumentCaptor.forClass(GetConsumptionUseCase.GetConsumptionCallback.class);
    verify(mockGetConsumptionUseCase).getConsumption(
        getConsumptionCallbackArgumentCaptor.capture());

    // call back
    getConsumptionCallbackArgumentCaptor.getValue().onError(mockErrorBundle);

    // verify
    verify(mockConsumptionView).showError(contains(errorMessage));
    verify(mockConsumptionView, never()).renderConsumption(any(Consumption.class));
  }

  @Test public void takeMealShouldSuccess() throws Exception {
    final Meal mockMeal = mock(Meal.class);
    final Consumed mockConsumed = mock(Consumed.class, new Answer() {
      @Override public Object answer(InvocationOnMock invocation) throws Throwable {
        return Calendar.getInstance();
      }
    });

    // exercise
    consumptionPresenter.takeMeal(mockMeal);

    ArgumentCaptor<TakeMealUseCase.TakeMealCallback> takeMealCallbackArgumentCaptor =
        ArgumentCaptor.forClass(TakeMealUseCase.TakeMealCallback.class);
    verify(mockTakeMealUseCase).takeMeal(eq(mockMeal), takeMealCallbackArgumentCaptor.capture());

    // call back
    takeMealCallbackArgumentCaptor.getValue().onMealTaken(mockConsumed);
    verify(mockConsumptionView).showSuccess(contains("Meal taken at "));
    verify(mockConsumptionView, never()).cancelMealTake(any(Meal.class));
  }

  @Test public void takeMealShouldShowErrorWhenFailure() throws Exception {
    final Meal mockMeal = mock(Meal.class);
    final Context mockContext = mock(Context.class);
    when(mockConsumptionView.getContext()).thenReturn(mockContext);

    ErrorBundle mockErrorBundle = mock(ErrorBundle.class);
    final String errorMessage = "test error message";
    when(mockErrorBundle.getErrorMessage()).thenReturn(errorMessage);
    final Exception mockException = mock(Exception.class);
    when(mockException.getLocalizedMessage()).thenReturn(errorMessage);
    when(mockErrorBundle.getException()).thenReturn(mockException);

    // exercise
    consumptionPresenter.takeMeal(mockMeal);

    ArgumentCaptor<TakeMealUseCase.TakeMealCallback> takeMealCallbackArgumentCaptor =
        ArgumentCaptor.forClass(TakeMealUseCase.TakeMealCallback.class);
    verify(mockTakeMealUseCase).takeMeal(eq(mockMeal), takeMealCallbackArgumentCaptor.capture());

    takeMealCallbackArgumentCaptor.getValue().onError(mockErrorBundle);
    verify(mockConsumptionView).showError(contains(errorMessage));
    verify(mockConsumptionView).cancelMealTake(mockMeal);
    verify(mockConsumptionView, never()).showSuccess(anyString());
  }
}