/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.data.repository;

import com.google.gson.Gson;
import com.solipaste.canitake.domain.Consumable;
import com.solipaste.canitake.domain.Consumed;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.Meal;
import com.solipaste.canitake.domain.Medication;
import java.util.Calendar;
import org.assertj.core.api.iterable.Extractor;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

public class ConsumptionEntityMapperTest {

  private final String CONSUMPTION_EMPTY_JSON = "{}";

  private final String CONSUMPTION_ONLY_USERNAME_JSON = "{\"user_name\": \"anonymous\"}";

  private final String CONSUMPTION_EMPTY_CONSUMED_JSON = "{\"consumed\": []}";

  private final String CONSUMPTION_THREE_CONSUMABLE_JSON = "{\n" +
      "\"user_name\": \"Mike\",\n" +
      "\"consumed\": [\n" +
      "{unixTime: 1443093514582, name: \"Dinner\", type:\"meal\"},\n" +
      "{unixTime: 1443093514583, name: \"Supper\", type:\"meal\"},\n" +
      "{unixTime: 1443093514584, type:\"medication\", name: \"Vitamin\"}\n" +
      "]\n" +
      "}\n";

  Extractor<Consumed, Consumable> consumed = new Extractor<Consumed, Consumable>() {
    @Override public Consumable extract(Consumed input) {
      return input.getConsumable();
    }
  };
  Extractor<Consumed, Long> consumedAt = new Extractor<Consumed, Long>() {
    @Override public Long extract(Consumed input) {
      return input.getTime().getTimeInMillis();
    }
  };

  private ConsumptionEntityMapper consumptionEntityMapper;

  @Before public void setUp() throws Exception {
    Gson gson = new Gson();
    consumptionEntityMapper = new ConsumptionEntityMapper(gson);
    assertThat(consumptionEntityMapper).isNotNull();
  }

  @Test public void shouldMapEmptyFromJSON() throws Exception {
    final ConsumptionEntity entity = consumptionEntityMapper.transformConsumptionEntity(CONSUMPTION_EMPTY_JSON);
    assertThat(entity.getUserName()).isEmpty();
    assertThat(entity.getConsumed()).isEmpty();
  }

  @Test public void shouldMapEmptyConsumedFromJSON() throws Exception {
    final ConsumptionEntity entity = consumptionEntityMapper.transformConsumptionEntity(CONSUMPTION_EMPTY_JSON);
    assertThat(entity.getUserName()).isEmpty();
    assertThat(entity.getConsumed()).isEmpty();
  }

  @Test public void shouldMapOnlyUserNameFromJSON() throws Exception {
    ConsumptionEntity entity = consumptionEntityMapper.transformConsumptionEntity(CONSUMPTION_ONLY_USERNAME_JSON);
    assertThat(entity.getUserName()).isEqualTo("anonymous");
    assertThat(entity.getConsumed()).isEmpty();
  }

  @Test public void shouldMapThreeConsumedFromJSON() throws Exception {
    ConsumptionEntity entity =
        consumptionEntityMapper.transformConsumptionEntity(CONSUMPTION_THREE_CONSUMABLE_JSON);
    assertThat(entity.getUserName()).isEqualTo("Mike");
    assertThat(entity.getConsumed()).hasSize(3)
        .extracting("type", "name")
        .contains(tuple(ConsumedEntity.ConsumedType.MEAL, "Dinner"),
            tuple(ConsumedEntity.ConsumedType.MEAL, "Supper"),
            tuple(ConsumedEntity.ConsumedType.MEDICATION, "Vitamin"));
    assertThat(entity.getConsumed()).hasSize(3)
        .extracting("unixTime")
        .containsExactly(1443093514582l, 1443093514583l, 1443093514584l);
  }

  @Test public void shouldMapOneConsumedToJSON() throws Exception {
    ConsumptionEntity entity = new ConsumptionEntity("Mike");
    final Calendar now = Calendar.getInstance();
    entity.addConsumed(new ConsumedEntity(now, ConsumedEntity.ConsumedType.MEAL, "Supper"));
    String json = consumptionEntityMapper.transformConsumptionEntity(entity);
    JSONAssert.assertEquals("{user_name:Mike, consumed: [{unixTime:" +
            String.valueOf(now.getTimeInMillis()) + ", type:meal, name:Supper}]}", json, false);
  }

  @Test public void shouldMapSeveralConsumedToJSON() throws Exception {
    ConsumptionEntity entity = new ConsumptionEntity("Mike");
    final Calendar now = Calendar.getInstance();
    final Calendar yesterday = Calendar.getInstance();
    yesterday.add(Calendar.DAY_OF_YEAR, -1);
    entity.addConsumed(new ConsumedEntity(yesterday, ConsumedEntity.ConsumedType.MEAL, "Supper"));
    entity.addConsumed(new ConsumedEntity(now, ConsumedEntity.ConsumedType.MEDICATION, "Vitamin"));
    String json = consumptionEntityMapper.transformConsumptionEntity(entity);
    JSONAssert.assertEquals("{user_name:Mike, consumed: ["
            +
            "{unixTime:"
            + String.valueOf(yesterday.getTimeInMillis())
            + ", type:meal, name:Supper}, "
            +
            "{unixTime:"
            + String.valueOf(now.getTimeInMillis())
            + ", type:medication, name:Vitamin}]}", json, false);
  }

  @Test public void shouldMapThreeConsumptionEntityToConsumption() throws Exception {
    ConsumptionEntity entity =
        consumptionEntityMapper.transformConsumptionEntity(CONSUMPTION_THREE_CONSUMABLE_JSON);
    Consumption consumption = consumptionEntityMapper.transformConsumption(entity);
    assertThat(consumption.getConsumed()).hasSize(3);
    assertThat(consumption.getConsumed()).extracting(consumed)
        .containsExactly(new Meal("Dinner"), new Meal("Supper"), new Medication("Vitamin"));
    assertThat(consumption.getConsumed()).extracting(consumedAt)
        .containsExactly(1443093514582l, 1443093514583l, 1443093514584l);
  }

  @Test public void shouldMapEmptyConsumptionEntityToConsumption() throws Exception {
    ConsumptionEntity entity = new ConsumptionEntity();
    Consumption consumption = consumptionEntityMapper.transformConsumption(entity);
    assertThat(consumption.getConsumed()).isEmpty();
  }

  @Test public void shouldMapEmptyConsumptionToConsumptionEntity() throws Exception {
    Consumption consumption = new Consumption();
    ConsumptionEntity entity = consumptionEntityMapper.transformConsumptionEntity(consumption);
    assertThat(entity.getUserName()).isEmpty();
  }

  @Test public void shouldMapConsumptionToConsumptionEntity() throws Exception {
    Consumption mConsumption = new Consumption();
    final Consumed mealEntry = mConsumption.consume(new Meal("Lunch", 60));
    final Consumed ironEntry = mConsumption.consume(new Medication("Iron", 120));
    final Consumed dinnerEntry = mConsumption.consume(new Meal("Dinner"));
    final Consumed superVitaminEntry = mConsumption.consume(new Medication("SuperVitamin"));

    final ConsumptionEntity entity = consumptionEntityMapper.transformConsumptionEntity(mConsumption);
    assertThat(entity.getConsumed()).hasSize(4)
        .extracting("unixTime", "type", "name")
        .containsExactly(
            tuple(mealEntry.getTime().getTimeInMillis(), ConsumedEntity.ConsumedType.MEAL, "Lunch"),
            tuple(ironEntry.getTime().getTimeInMillis(), ConsumedEntity.ConsumedType.MEDICATION,
                "Iron"),
            tuple(dinnerEntry.getTime().getTimeInMillis(), ConsumedEntity.ConsumedType.MEAL,
                "Dinner"), tuple(superVitaminEntry.getTime().getTimeInMillis(),
                ConsumedEntity.ConsumedType.MEDICATION, "SuperVitamin"));
  }
}