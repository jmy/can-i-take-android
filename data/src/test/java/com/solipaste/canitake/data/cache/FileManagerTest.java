/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.data.cache;

import android.content.Context;
import android.content.SharedPreferences;
import com.solipaste.canitake.data.ApplicationStub;
import com.solipaste.canitake.data.BuildConfig;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(RobolectricGradleTestRunner.class) @Config(constants = BuildConfig.class, sdk = 21,
    application = ApplicationStub.class)

public class FileManagerTest {
  private static final String SETTINGS_FILE_NAME = "com.solipaste.canitake.SETTINGS.test";

  private FileManager fileManager;
  private File cacheDir;
  private String fileContent = "TestRunner was here.\n" + "and still is.";
  private String prefsFileName = "prefsFile";
  private Context context;
  private SharedPreferences sharedPreferences;

  @Before public void setUp() throws Exception {
    context = RuntimeEnvironment.application;
    sharedPreferences = context.getSharedPreferences(SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
    fileManager = new FileManager(sharedPreferences);
    cacheDir = context.getCacheDir();

    assertThat(cacheDir.exists()).isTrue();
    File[] cachedFiles = cacheDir.listFiles();
    assertThat(cachedFiles).isEmpty();

    deletePrefs(prefsFileName);
  }

  @After public void tearDown() throws Exception {
    deleteDir(cacheDir);
    deletePrefs(prefsFileName);
  }

  @Test public void shouldWriteToNewFile() throws Exception {
    final File file = buildFile("testfile");
    assertThat(fileManager.writeToFile(file, fileContent, false)).isTrue();
    assertThat(readFile(file)).isEqualTo(fileContent);
  }

  @Test public void shouldNotOverwriteExistingFile() throws Exception {
    final File file = buildFile("testfile");
    final String oldContent = "not overwritten";
    writeFile(file, oldContent);
    assertThat(file.exists()).isTrue();
    assertThat(fileManager.writeToFile(file, fileContent, false)).isFalse();
    assertThat(readFile(file)).isEqualTo(oldContent);
  }

  @Test public void shouldOverwriteExistingFileWhenAsked() throws Exception {
    final File file = buildFile("testfile");
    final String oldContent = "not overwritten";
    writeFile(file, oldContent);
    assertThat(file.exists()).isTrue();
    assertThat(fileManager.writeToFile(file, fileContent, true)).isTrue();
    assertThat(readFile(file)).isEqualTo(fileContent);
  }

  @Test public void shouldReadFileContentSuccessfully() throws Exception {
    final File file = buildFile("testfile");
    writeFile(file, fileContent);
    assertThat(fileManager.readFileContent(file)).isEqualTo(fileContent);
  }

  @Test public void shouldReturnEmptyStringWhenReadingMissingFile() throws Exception {
    final File file = buildFile("nonexistingfile");
    assertThat(fileManager.readFileContent(file)).isEmpty();
  }

  @Test public void shouldReturnEmptyStringWhenReadingEmptyFile() throws Exception {
    final File file = buildFile("emptyFile");
    writeFile(file, "");
    assertThat(fileManager.readFileContent(file)).isEmpty();
  }

  @Test public void shouldNotFindNonExistingFile() throws Exception {
    final File file = buildFile("testfile");
    assertThat(fileManager.exists(file)).isFalse();
  }

  @Test public void shouldFindExistingFile() throws Exception {
    final File file = buildFile("testfile");
    file.createNewFile();
    assertThat(fileManager.exists(file)).isTrue();
  }

  @Test public void clearDirectory() throws Exception {
    final File file = buildFile("testfile");
    file.createNewFile();
    fileManager.clearDirectory(cacheDir);
    assertThat(file.exists()).isFalse();
  }

  @Test public void writeToPreferences() throws Exception {
    final String keyName = "keyToWrite";
    fileManager.writeToPreferences(keyName, 666);
    assertThat(sharedPreferences.getLong(keyName, 0)).isEqualTo(666);
  }

  @Test public void getFromPreferences() throws Exception {
    final SharedPreferences.Editor editor = sharedPreferences.edit();
    final String keyName = "keyToGet";
    editor.putLong(keyName, 999);
    editor.commit();
    assertThat(fileManager.getFromPreferences(keyName)).isEqualTo(999);
  }

  private File buildFile(String name) {
    StringBuilder fileNameBuilder = new StringBuilder();
    fileNameBuilder.append(cacheDir.getPath());
    fileNameBuilder.append(File.separator);
    fileNameBuilder.append(name);

    return new File(fileNameBuilder.toString());
  }

  private void writeFile(File file, String content) throws IOException {
    FileWriter writer = null;
    try {
      writer = new FileWriter(file);
      writer.write(content);
    } catch (IOException e) {
      throw new AssertionError(e);
    } finally {
      writer.close();
    }
  }

  private String readFile(File file) {
    StringBuilder fileContentBuilder = new StringBuilder();
    try {
      FileReader fileReader = new FileReader(file);
      BufferedReader bufferedReader = new BufferedReader(fileReader);
      String stringLine;
      while ((stringLine = bufferedReader.readLine()) != null) {
        if (fileContentBuilder.length() > 0) {
          fileContentBuilder.append(System.getProperty("line.separator"));
        }
        fileContentBuilder.append(stringLine);
      }
      bufferedReader.close();
      fileReader.close();
    } catch (Exception e) {
      throw new AssertionError(e);
    }
    return fileContentBuilder.toString();
  }

  public void deleteDir(File path) {
    if (path.isDirectory()) {
      File[] files = path.listFiles();
      assertThat(files).isNotNull();
      for (File f : files) {
        deleteDir(f);
      }
    }
    path.delete();
  }

  private void deletePrefs(String prefsFileName) {
    final SharedPreferences.Editor editor = sharedPreferences.edit();
    editor.clear();
    editor.commit();
  }
}