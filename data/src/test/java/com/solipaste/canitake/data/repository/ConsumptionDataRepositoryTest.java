/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.data.repository;

import com.solipaste.canitake.data.cache.ConsumptionCacheImpl;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.exception.ErrorBundle;
import com.solipaste.canitake.domain.repository.ConsumptionRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class ConsumptionDataRepositoryTest {

  @Mock private ConsumptionRepository.ConsumptionLoadCallback mockConsumptionLoadRepositoryCallback;
  @Mock private ConsumptionRepository.ConsumptionSaveCallback mockConsumptionSaveRepositoryCallback;
  @Mock private ConsumptionCacheImpl mockConsumptionCache;
  @Mock private ConsumptionEntityMapper mockConsumptionEntityMapper;

  private ConsumptionDataRepository consumptionDataRepository;

  @Before public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    consumptionDataRepository =
        new ConsumptionDataRepository(mockConsumptionCache, mockConsumptionEntityMapper);
  }

  @Test public void getConsumptionShouldCallbackSuccessfully() throws Exception {
    final ConsumptionDataRepository consumptionDataRepository = this.consumptionDataRepository;
    consumptionDataRepository.getConsumption(mockConsumptionLoadRepositoryCallback);
    verify(mockConsumptionLoadRepositoryCallback).onConsumptionLoaded(any(Consumption.class));
    verify(mockConsumptionLoadRepositoryCallback, never()).onError(any(ErrorBundle.class));
  }

  @Test public void putConsumptionShouldCallbackSuccessfully() throws Exception {
    final ConsumptionDataRepository consumptionDataRepository = this.consumptionDataRepository;
    Consumption mockConsumption = mock(Consumption.class);
    consumptionDataRepository.putConsumption(mockConsumption, mockConsumptionSaveRepositoryCallback);
    verify(mockConsumptionCache).put(any(ConsumptionEntity.class));
    verify(mockConsumptionEntityMapper).transformConsumptionEntity(mockConsumption);
    verify(mockConsumptionSaveRepositoryCallback).onConsumptionSaved();
    verify(mockConsumptionSaveRepositoryCallback, never()).onError(any(ErrorBundle.class));
  }
}