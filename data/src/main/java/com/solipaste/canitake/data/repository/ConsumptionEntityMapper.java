/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.data.repository;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.solipaste.canitake.domain.Consumed;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.Meal;
import com.solipaste.canitake.domain.Medication;
import edu.umd.cs.findbugs.annotations.NonNull;
import java.lang.reflect.Type;
import java.util.Calendar;
import javax.inject.Inject;


/**
 * Class used to transform from Strings representing json to valid objects.
 */
public class ConsumptionEntityMapper {

  private final Gson gson;

  @Inject public ConsumptionEntityMapper(Gson gson) {
    this.gson = gson;
  }

  /**
   * Transform from valid json string to {@link ConsumptionEntity}.
   *
   * @param consumptionJsonResponse A json representing a user profile.
   * @return {@link ConsumptionEntity}.
   * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
   */
  public ConsumptionEntity transformConsumptionEntity(String consumptionJsonResponse)
      throws JsonSyntaxException {
    try {
      Type consumptionEntityType = new TypeToken<ConsumptionEntity>() {
      }.getType();
      ConsumptionEntity consumptionEntity =
          gson.fromJson(consumptionJsonResponse, consumptionEntityType);

      return consumptionEntity;
    } catch (JsonSyntaxException jsonException) {
      throw jsonException;
    }
  }


  public String transformConsumptionEntity(ConsumptionEntity entity) {
    return gson.toJson(entity);
  }

  /**
   * Transform from Consumption to {@link ConsumptionEntity}.
   *
   * @param consumption A consumption.
   * @return {@link ConsumptionEntity}.
   */
  public ConsumptionEntity transformConsumptionEntity(@NonNull Consumption consumption) {
    final ConsumptionEntity entity = new ConsumptionEntity();
    for (Consumed consumed : consumption.getConsumed()) {
      if (consumed.getConsumable() instanceof Meal) {
        entity.addConsumed(new com.solipaste.canitake.data.repository.ConsumedEntity(
            consumed.getTime().getTimeInMillis(),
            com.solipaste.canitake.data.repository.ConsumedEntity.ConsumedType.MEAL,
            consumed.getConsumable().getName()));
      } else if (consumed.getConsumable() instanceof Medication) {
        entity.addConsumed(new com.solipaste.canitake.data.repository.ConsumedEntity(
            consumed.getTime().getTimeInMillis(),
            com.solipaste.canitake.data.repository.ConsumedEntity.ConsumedType.MEDICATION,
            consumed.getConsumable().getName()));
      } else {
        throw new IllegalArgumentException("Unknown Consumable");
      }
    }
    return entity;
  }

  /**
   * Transform from Consumption to {@link ConsumptionEntity}.
   *
   * @param entity A consumptionEntity.
   * @return {@link Consumption}.
   */
  public Consumption transformConsumption(@NonNull ConsumptionEntity entity) {
    final Consumption consumption = new Consumption();
    for (ConsumedEntity consumed : entity.getConsumed()) {
      final Calendar time = Calendar.getInstance();
      time.setTimeInMillis(consumed.getUnixTime());
      switch (consumed.getType()) {
        case MEAL:
          consumption.consumeAt(new Meal(consumed.getName()), time);
          break;
        case MEDICATION:
          consumption.consumeAt(new Medication(consumed.getName()), time);
          break;
        default:
          throw new IllegalArgumentException("Unknown Consumable");
      }
    }
    return consumption;
  }
}

