/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.data.cache;

import com.solipaste.canitake.data.repository.ConsumptionEntity;

/**
 * An interface representing a Consumption Cache.
 */
public interface ConsumptionCache {

  /**
   * Gets element from the cache
   *
   * @return ConsumptionEntity
   */
  ConsumptionEntity get();

  /**
   * Puts and element into the cache.
   *
   * @param consumptionEntity Element to insert in the cache.
   */
  void put(ConsumptionEntity consumptionEntity);

  /**
   * Checks if an element (Consumption) exists in the cache.
   *
   * @return true if the element is cached, otherwise false.
   */
  boolean isCached();

  /**
   * Checks if the cache is expired.
   *
   * @return true, the cache is expired, otherwise false.
   */
  boolean isExpired();

  /**
   * Evict all elements of the cache.
   */
  void evictAll();
}
