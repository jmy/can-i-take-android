/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.data.cache;

import android.content.Context;
import com.solipaste.canitake.data.repository.ConsumptionEntity;
import com.solipaste.canitake.data.repository.ConsumptionEntityMapper;
import edu.umd.cs.findbugs.annotations.NonNull;
import edu.umd.cs.findbugs.annotations.Nullable;
import java.io.File;
import javax.inject.Inject;

/**
 * Concrete implementation class for {@link ConsumptionCache}. Stores consumption in a file on
 * Android device. The storage format is specified by {@link ConsumptionEntityMapper}.
 */
public class ConsumptionCacheImpl implements ConsumptionCache {

  private static final String SETTINGS_KEY_LAST_CACHE_UPDATE = "last_cache_update";

  private static final String DEFAULT_FILE_NAME = "consumption";
  private static final long EXPIRATION_TIME = 60 * 10 * 1000;

  private final Context context;
  private final File cacheDir;
  private final FileManager fileManager;
  private final ConsumptionEntityMapper mapper;

  /**
   * Constructs new {@code ConsumptionCacheImpl}.
   *
   * @param context a valid context used for file location
   * @param mapper mapper used to map entities to strings and vice-versa
   * @param fileManager {@code FileManager} used for storing the files
   */
  @Inject
  public ConsumptionCacheImpl(@NonNull Context context, @NonNull ConsumptionEntityMapper mapper,
      @NonNull FileManager fileManager) {
    this.context = context.getApplicationContext();
    cacheDir = this.context.getCacheDir();
    this.fileManager = fileManager;
    this.mapper = mapper;
  }

  /**
   * Returns {@code ConsumptionEntity}
   *
   * @return ConsumptionEntity found
   */
  @Override @Nullable public ConsumptionEntity get() {
    File consumptionEntityFile = buildFile();
    String fileContent = fileManager.readFileContent(consumptionEntityFile);
    if (fileContent.length() > 0) {
      ConsumptionEntity consumptionEntity = mapper.transformConsumptionEntity(fileContent);
      return consumptionEntity;
    } else {
      return null;
    }
  }

  /**
   * Stores {@code ConsumptionEntity} to the cache
   *
   * @param consumptionEntity element to insert in the cache.
   */
  @Override public void put(@NonNull ConsumptionEntity consumptionEntity) {
    final File entityFile = buildFile();
    String jsonString = mapper.transformConsumptionEntity(consumptionEntity);
    final boolean overwrite = true;
    fileManager.writeToFile(entityFile, jsonString, overwrite);
    setLastCacheUpdateTimeMillis();
  }

  /**
   * Checks if cache exists
   *
   * @return {@code true} is cache exists, {@code false} otherwise
   */
  @Override public boolean isCached() {
    final File file = buildFile();
    return fileManager.exists(file);
  }

  /**
   * Expriration is not currently implemented. Cache never expires.
   *
   * @return false always
   */
  @Override public boolean isExpired() {
    return false;
  }

  /**
   * Clears all cached data.
   */
  @Override public void evictAll() {
    fileManager.clearDirectory(cacheDir);
  }

  /**
   * Set in millis, the last time the cache was accessed.
   */
  private void setLastCacheUpdateTimeMillis() {
    long currentMillis = System.currentTimeMillis();
    fileManager.writeToPreferences(SETTINGS_KEY_LAST_CACHE_UPDATE, currentMillis);
  }

  /**
   * Get in millis, the last time the cache was accessed.
   */
  private long getLastCacheUpdateTimeMillis() {
    return fileManager.getFromPreferences(SETTINGS_KEY_LAST_CACHE_UPDATE);
  }

  /**
   * Build a file, used to be inserted in the disk cache.
   *
   * @return A valid file.
   */
  private File buildFile() {

    return new File(this.cacheDir.getPath() + File.separator + DEFAULT_FILE_NAME);
  }
}
