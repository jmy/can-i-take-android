/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.data.repository;

import com.solipaste.canitake.data.cache.ConsumptionCache;
import com.solipaste.canitake.domain.Consumption;
import com.solipaste.canitake.domain.Meal;
import com.solipaste.canitake.domain.repository.ConsumptionRepository;
import edu.umd.cs.findbugs.annotations.NonNull;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * {@code ConsumptionDataRepository} implements a permanent, Android specific storage for {@link
 * Consumption}. Implemented as a singleton.
 */
@Singleton public class ConsumptionDataRepository implements ConsumptionRepository {

  @NonNull private static ConsumptionCache cache;
  @NonNull private static ConsumptionEntityMapper mapper;
  private Consumption consumption;

  /**
   * Creates concrete {@code ConsumptionDataRepository} object for storing Consumptions.
   * @param cache cache implementation used for storage.
   * @param mapper mapper implementation used for mapping domain module objects into data module
   * objects.
   */
  @Inject public ConsumptionDataRepository(@NonNull ConsumptionCache cache,
      @NonNull ConsumptionEntityMapper mapper) {
    ConsumptionDataRepository.cache = cache;
    ConsumptionDataRepository.mapper = mapper;
  }

  /**
   * Returns Consumption from storage by calling the callback provided as a argument.
   *
   * @param consumptionLoadCallback callback called to return consumption or error.
   */
  @Override
  public void getConsumption(@NonNull final ConsumptionLoadCallback consumptionLoadCallback) {
    final ConsumptionEntity consumptionEntity = cache.get();
    if (consumptionEntity != null) {
      consumption = mapper.transformConsumption(consumptionEntity);
      consumptionLoadCallback.onConsumptionLoaded(consumption);
    } else {
      consumptionLoadCallback.onConsumptionLoaded(new Consumption());
    }
    // TODO: 24.9.15 Handle errors?
  }

  /**
   * Saves the provided Consumption into persistent storage.
   *
   * @param consumption Consumption to be stored.
   * @param consumptionSaveCallback callback used to notify caller about the result of the
   * operation.
   */
  @Override public void putConsumption(@NonNull Consumption consumption,
      @NonNull final ConsumptionSaveCallback consumptionSaveCallback) {
    final ConsumptionEntity consumptionEntity = mapper.transformConsumptionEntity(consumption);
    cache.put(consumptionEntity);
    consumptionSaveCallback.onConsumptionSaved();
  }
}
