/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.canitake.data.repository;

import com.google.gson.annotations.SerializedName;
import java.util.Calendar;

public class ConsumedEntity {

  @SerializedName("unixTime") private long unixTime;
  @SerializedName("type") private ConsumedType type;
  @SerializedName("name") private String name;

  public ConsumedEntity() {
  }

  public ConsumedEntity(long unixTime, ConsumedType type, String name) {
    this.unixTime = unixTime;
    this.type = type;
    this.name = name;
  }

  public ConsumedEntity(Calendar unixTime, ConsumedType type, String name) {
    this.unixTime = unixTime.getTimeInMillis();
    this.type = type;
    this.name = name;
  }

  public long getUnixTime() {
    return unixTime;
  }

  public ConsumedType getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  @Override public String toString() {
    return "ConsumedEntity{" +
        "unixTime=" + unixTime +
        ", type=" + type +
        ", name='" + name + '\'' +
        '}';
  }

  public enum ConsumedType {
    @SerializedName("meal")
    MEAL,
    @SerializedName("medication")
    MEDICATION
  }
}
